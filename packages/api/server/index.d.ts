import { GraphQLResolveInfo } from 'graphql';
export declare type Maybe<T> = T | null;
export declare type RequireFields<T, K extends keyof T> = {
    [X in Exclude<keyof T, K>]?: T[X];
} & {
    [P in K]-?: NonNullable<T[P]>;
};
/** All built-in and custom scalars, mapped to their actual values */
export declare type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
};
export declare type AdditionalEntityFields = {
    path?: Maybe<Scalars['String']>;
    type?: Maybe<Scalars['String']>;
};
export declare type Exercise = {
    __typename?: 'Exercise';
    id: Scalars['ID'];
    userId: Scalars['ID'];
    name: Scalars['String'];
    sets?: Maybe<Array<Sets>>;
};
export declare type ExerciseInput = {
    id: Scalars['ID'];
    name: Scalars['String'];
    sets?: Maybe<Array<SetsInput>>;
    userId: Scalars['ID'];
};
export declare type Mutation = {
    __typename?: 'Mutation';
    login?: Maybe<Token>;
    registerWorkout?: Maybe<Workout>;
    signUp?: Maybe<Token>;
};
export declare type MutationLoginArgs = {
    username: Scalars['String'];
    password: Scalars['String'];
};
export declare type MutationRegisterWorkoutArgs = {
    name: Scalars['String'];
    date: Scalars['String'];
    userId: Scalars['ID'];
    duration: Scalars['Float'];
    exercises?: Maybe<Array<ExerciseInput>>;
};
export declare type MutationSignUpArgs = {
    email: Scalars['String'];
    username: Scalars['String'];
    name?: Maybe<Scalars['String']>;
    password: Scalars['String'];
};
export declare type Query = {
    __typename?: 'Query';
    getWorkoutById?: Maybe<Workout>;
    getWorkoutsByUserId: Array<Workout>;
    getWorkoutOverviewStats?: Maybe<WorkoutSummary>;
    getExercisesByUserId: Array<Exercise>;
    getUserById?: Maybe<User>;
    getAllUsers: Array<User>;
};
export declare type QueryGetWorkoutByIdArgs = {
    id: Scalars['ID'];
};
export declare type QueryGetWorkoutsByUserIdArgs = {
    userId: Scalars['ID'];
};
export declare type QueryGetWorkoutOverviewStatsArgs = {
    userId: Scalars['ID'];
};
export declare type QueryGetExercisesByUserIdArgs = {
    userId: Scalars['ID'];
};
export declare type QueryGetUserByIdArgs = {
    id: Scalars['ID'];
};
export declare enum Role {
    Admin = "ADMIN",
    User = "USER"
}
export declare type Sets = {
    __typename?: 'Sets';
    repetitions: Scalars['Int'];
    weight: Scalars['Float'];
    type: SetType;
};
export declare type SetsInput = {
    repetitions: Scalars['Int'];
    weight: Scalars['Float'];
    type: SetType;
};
export declare enum SetType {
    Normal = "NORMAL",
    Warmup = "WARMUP",
    Failure = "FAILURE"
}
export declare type Token = {
    __typename?: 'Token';
    token: Scalars['String'];
};
export declare type User = {
    __typename?: 'User';
    id?: Maybe<Scalars['ID']>;
    name?: Maybe<Scalars['String']>;
    username: Scalars['String'];
    password: Scalars['String'];
    email: Scalars['String'];
    createdAt: Scalars['String'];
    roles: Array<Role>;
    workouts?: Maybe<Array<Workout>>;
    exercises?: Maybe<Array<Exercise>>;
};
export declare type Workout = {
    __typename?: 'Workout';
    id: Scalars['ID'];
    userId: Scalars['ID'];
    name: Scalars['String'];
    date: Scalars['String'];
    createdAt: Scalars['String'];
    exercises?: Maybe<Array<Exercise>>;
    duration: Scalars['Float'];
};
export declare type WorkoutSummary = {
    __typename?: 'WorkoutSummary';
    workoutCount: Scalars['Int'];
    duration: Scalars['Float'];
};
export declare type ResolverTypeWrapper<T> = Promise<T> | T;
export declare type ResolverFn<TResult, TParent, TContext, TArgs> = (parent: TParent, args: TArgs, context: TContext, info: GraphQLResolveInfo) => Promise<TResult> | TResult;
export declare type StitchingResolver<TResult, TParent, TContext, TArgs> = {
    fragment: string;
    resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export declare type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> = ResolverFn<TResult, TParent, TContext, TArgs> | StitchingResolver<TResult, TParent, TContext, TArgs>;
export declare type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (parent: TParent, args: TArgs, context: TContext, info: GraphQLResolveInfo) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;
export declare type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (parent: TParent, args: TArgs, context: TContext, info: GraphQLResolveInfo) => TResult | Promise<TResult>;
export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
    subscribe: SubscriptionSubscribeFn<{
        [key in TKey]: TResult;
    }, TParent, TContext, TArgs>;
    resolve?: SubscriptionResolveFn<TResult, {
        [key in TKey]: TResult;
    }, TContext, TArgs>;
}
export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
    subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
    resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}
export declare type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> = SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs> | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;
export declare type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> = ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>) | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;
export declare type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (parent: TParent, context: TContext, info: GraphQLResolveInfo) => Maybe<TTypes>;
export declare type isTypeOfResolverFn<T = {}> = (obj: T, info: GraphQLResolveInfo) => boolean;
export declare type NextResolverFn<T> = () => Promise<T>;
export declare type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (next: NextResolverFn<TResult>, parent: TParent, args: TArgs, context: TContext, info: GraphQLResolveInfo) => TResult | Promise<TResult>;
/** Mapping between all available schema types and the resolvers types */
export declare type ResolversTypes = {
    Query: ResolverTypeWrapper<{}>;
    ID: ResolverTypeWrapper<Scalars['ID']>;
    Workout: ResolverTypeWrapper<Workout>;
    String: ResolverTypeWrapper<Scalars['String']>;
    Exercise: ResolverTypeWrapper<Exercise>;
    Sets: ResolverTypeWrapper<Sets>;
    Int: ResolverTypeWrapper<Scalars['Int']>;
    Float: ResolverTypeWrapper<Scalars['Float']>;
    SetType: SetType;
    WorkoutSummary: ResolverTypeWrapper<WorkoutSummary>;
    User: ResolverTypeWrapper<User>;
    Role: Role;
    Mutation: ResolverTypeWrapper<{}>;
    Token: ResolverTypeWrapper<Token>;
    ExerciseInput: ExerciseInput;
    SetsInput: SetsInput;
    Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
    AdditionalEntityFields: AdditionalEntityFields;
};
/** Mapping between all available schema types and the resolvers parents */
export declare type ResolversParentTypes = {
    Query: {};
    ID: Scalars['ID'];
    Workout: Workout;
    String: Scalars['String'];
    Exercise: Exercise;
    Sets: Sets;
    Int: Scalars['Int'];
    Float: Scalars['Float'];
    SetType: SetType;
    WorkoutSummary: WorkoutSummary;
    User: User;
    Role: Role;
    Mutation: {};
    Token: Token;
    ExerciseInput: ExerciseInput;
    SetsInput: SetsInput;
    Boolean: Scalars['Boolean'];
    AdditionalEntityFields: AdditionalEntityFields;
};
export declare type RoleDirectiveArgs = {
    roles: Array<Role>;
};
export declare type RoleDirectiveResolver<Result, Parent, ContextType = any, Args = RoleDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;
export declare type AuthDirectiveArgs = {};
export declare type AuthDirectiveResolver<Result, Parent, ContextType = any, Args = AuthDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;
export declare type UnionDirectiveArgs = {
    discriminatorField?: Maybe<Scalars['String']>;
    additionalFields?: Maybe<Array<Maybe<AdditionalEntityFields>>>;
};
export declare type UnionDirectiveResolver<Result, Parent, ContextType = any, Args = UnionDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;
export declare type AbstractEntityDirectiveArgs = {
    discriminatorField: Scalars['String'];
    additionalFields?: Maybe<Array<Maybe<AdditionalEntityFields>>>;
};
export declare type AbstractEntityDirectiveResolver<Result, Parent, ContextType = any, Args = AbstractEntityDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;
export declare type EntityDirectiveArgs = {
    embedded?: Maybe<Scalars['Boolean']>;
    additionalFields?: Maybe<Array<Maybe<AdditionalEntityFields>>>;
};
export declare type EntityDirectiveResolver<Result, Parent, ContextType = any, Args = EntityDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;
export declare type ColumnDirectiveArgs = {
    overrideType?: Maybe<Scalars['String']>;
};
export declare type ColumnDirectiveResolver<Result, Parent, ContextType = any, Args = ColumnDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;
export declare type IdDirectiveArgs = {};
export declare type IdDirectiveResolver<Result, Parent, ContextType = any, Args = IdDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;
export declare type LinkDirectiveArgs = {
    overrideType?: Maybe<Scalars['String']>;
};
export declare type LinkDirectiveResolver<Result, Parent, ContextType = any, Args = LinkDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;
export declare type EmbeddedDirectiveArgs = {};
export declare type EmbeddedDirectiveResolver<Result, Parent, ContextType = any, Args = EmbeddedDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;
export declare type MapDirectiveArgs = {
    path: Scalars['String'];
};
export declare type MapDirectiveResolver<Result, Parent, ContextType = any, Args = MapDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;
export declare type ExerciseResolvers<ContextType = any, ParentType extends ResolversParentTypes['Exercise'] = ResolversParentTypes['Exercise']> = {
    id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
    userId?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
    name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    sets?: Resolver<Maybe<Array<ResolversTypes['Sets']>>, ParentType, ContextType>;
    __isTypeOf?: isTypeOfResolverFn<ParentType>;
};
export declare type MutationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = {
    login?: Resolver<Maybe<ResolversTypes['Token']>, ParentType, ContextType, RequireFields<MutationLoginArgs, 'username' | 'password'>>;
    registerWorkout?: Resolver<Maybe<ResolversTypes['Workout']>, ParentType, ContextType, RequireFields<MutationRegisterWorkoutArgs, 'name' | 'date' | 'userId' | 'duration'>>;
    signUp?: Resolver<Maybe<ResolversTypes['Token']>, ParentType, ContextType, RequireFields<MutationSignUpArgs, 'email' | 'username' | 'password'>>;
};
export declare type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = {
    getWorkoutById?: Resolver<Maybe<ResolversTypes['Workout']>, ParentType, ContextType, RequireFields<QueryGetWorkoutByIdArgs, 'id'>>;
    getWorkoutsByUserId?: Resolver<Array<ResolversTypes['Workout']>, ParentType, ContextType, RequireFields<QueryGetWorkoutsByUserIdArgs, 'userId'>>;
    getWorkoutOverviewStats?: Resolver<Maybe<ResolversTypes['WorkoutSummary']>, ParentType, ContextType, RequireFields<QueryGetWorkoutOverviewStatsArgs, 'userId'>>;
    getExercisesByUserId?: Resolver<Array<ResolversTypes['Exercise']>, ParentType, ContextType, RequireFields<QueryGetExercisesByUserIdArgs, 'userId'>>;
    getUserById?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType, RequireFields<QueryGetUserByIdArgs, 'id'>>;
    getAllUsers?: Resolver<Array<ResolversTypes['User']>, ParentType, ContextType>;
};
export declare type SetsResolvers<ContextType = any, ParentType extends ResolversParentTypes['Sets'] = ResolversParentTypes['Sets']> = {
    repetitions?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
    weight?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
    type?: Resolver<ResolversTypes['SetType'], ParentType, ContextType>;
    __isTypeOf?: isTypeOfResolverFn<ParentType>;
};
export declare type TokenResolvers<ContextType = any, ParentType extends ResolversParentTypes['Token'] = ResolversParentTypes['Token']> = {
    token?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    __isTypeOf?: isTypeOfResolverFn<ParentType>;
};
export declare type UserResolvers<ContextType = any, ParentType extends ResolversParentTypes['User'] = ResolversParentTypes['User']> = {
    id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>;
    name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
    username?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    password?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    email?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    createdAt?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    roles?: Resolver<Array<ResolversTypes['Role']>, ParentType, ContextType>;
    workouts?: Resolver<Maybe<Array<ResolversTypes['Workout']>>, ParentType, ContextType>;
    exercises?: Resolver<Maybe<Array<ResolversTypes['Exercise']>>, ParentType, ContextType>;
    __isTypeOf?: isTypeOfResolverFn<ParentType>;
};
export declare type WorkoutResolvers<ContextType = any, ParentType extends ResolversParentTypes['Workout'] = ResolversParentTypes['Workout']> = {
    id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
    userId?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
    name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    date?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    createdAt?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    exercises?: Resolver<Maybe<Array<ResolversTypes['Exercise']>>, ParentType, ContextType>;
    duration?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
    __isTypeOf?: isTypeOfResolverFn<ParentType>;
};
export declare type WorkoutSummaryResolvers<ContextType = any, ParentType extends ResolversParentTypes['WorkoutSummary'] = ResolversParentTypes['WorkoutSummary']> = {
    workoutCount?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
    duration?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
    __isTypeOf?: isTypeOfResolverFn<ParentType>;
};
export declare type Resolvers<ContextType = any> = {
    Exercise?: ExerciseResolvers<ContextType>;
    Mutation?: MutationResolvers<ContextType>;
    Query?: QueryResolvers<ContextType>;
    Sets?: SetsResolvers<ContextType>;
    Token?: TokenResolvers<ContextType>;
    User?: UserResolvers<ContextType>;
    Workout?: WorkoutResolvers<ContextType>;
    WorkoutSummary?: WorkoutSummaryResolvers<ContextType>;
};
/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
*/
export declare type IResolvers<ContextType = any> = Resolvers<ContextType>;
export declare type DirectiveResolvers<ContextType = any> = {
    role?: RoleDirectiveResolver<any, any, ContextType>;
    auth?: AuthDirectiveResolver<any, any, ContextType>;
    union?: UnionDirectiveResolver<any, any, ContextType>;
    abstractEntity?: AbstractEntityDirectiveResolver<any, any, ContextType>;
    entity?: EntityDirectiveResolver<any, any, ContextType>;
    column?: ColumnDirectiveResolver<any, any, ContextType>;
    id?: IdDirectiveResolver<any, any, ContextType>;
    link?: LinkDirectiveResolver<any, any, ContextType>;
    embedded?: EmbeddedDirectiveResolver<any, any, ContextType>;
    map?: MapDirectiveResolver<any, any, ContextType>;
};
/**
* @deprecated
* Use "DirectiveResolvers" root object instead. If you wish to get "IDirectiveResolvers", add "typesPrefix: I" to your config.
*/
export declare type IDirectiveResolvers<ContextType = any> = DirectiveResolvers<ContextType>;
export declare const typeDefs: string;
