"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Role;
(function (Role) {
    Role["Admin"] = "ADMIN";
    Role["User"] = "USER";
})(Role = exports.Role || (exports.Role = {}));
var SetType;
(function (SetType) {
    SetType["Normal"] = "NORMAL";
    SetType["Warmup"] = "WARMUP";
    SetType["Failure"] = "FAILURE";
})(SetType = exports.SetType || (exports.SetType = {}));
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
exports.typeDefs = fs_1.default.readFileSync(path_1.default.join(__dirname, './../schema.graphql'), { encoding: 'utf8' });
//# sourceMappingURL=index.js.map