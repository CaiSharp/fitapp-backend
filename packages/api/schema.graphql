directive @role(roles: [Role!]!) on FIELD_DEFINITION
directive @auth on FIELD_DEFINITION

enum Role {
	ADMIN
	USER
}

enum SetType {
	NORMAL
	WARMUP
	FAILURE
}

type Token {
	token: String!
}

type WorkoutSummary {
	workoutCount: Int!
	duration: Float!
}

type Workout {
	id: ID!
	userId: ID!
	name: String!
	date: String!
	createdAt: String!
	exercises: [Exercise!]
	duration: Float!
}

type Exercise {
	id: ID!
	userId: ID!
	name: String!
	sets: [Sets!]
}

type Sets {
	repetitions: Int!
	weight: Float!
	type: SetType!
}

type User {
	id: ID @role(roles: [ADMIN])
	name: String
	username: String!
	password: String!
	email: String!
	createdAt: String!
	roles: [Role!]!
	workouts: [Workout!]
	exercises: [Exercise!]
}

input ExerciseInput {
	id: ID!
	name: String!
	sets: [SetsInput!]
	userId: ID!
}

input SetsInput {
	repetitions: Int!
	weight: Float!
	type: SetType!
}

type Query {
	getWorkoutById(id: ID!): Workout
	getWorkoutsByUserId(userId: ID!): [Workout!]!
	getWorkoutOverviewStats(userId: ID!): WorkoutSummary
	getExercisesByUserId(userId: ID!): [Exercise!]!
	getUserById(id: ID!): User @auth
	getAllUsers: [User!]! @role(roles: [USER])
}

type Mutation {
	login(username: String!, password: String!): Token
	registerWorkout(
		name: String!
		date: String!
		userId: ID!
		duration: Float!
		exercises: [ExerciseInput!]
	): Workout
	signUp(
		email: String!
		username: String!
		name: String
		password: String!
	): Token
}
