var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
/*eslint-disable */
import gql from 'graphql-tag';
import * as React from 'react';
import * as ApolloReactComponents from '@apollo/react-components';
import * as ApolloReactHoc from '@apollo/react-hoc';
import * as ApolloReactHooks from '@apollo/react-hooks';
export var Role;
(function (Role) {
    Role["Admin"] = "ADMIN";
    Role["User"] = "USER";
})(Role || (Role = {}));
export var SetType;
(function (SetType) {
    SetType["Normal"] = "NORMAL";
    SetType["Warmup"] = "WARMUP";
    SetType["Failure"] = "FAILURE";
})(SetType || (SetType = {}));
export var LoginDocument = gql(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    mutation login($username: String!, $password: String!) {\n  login(username: $username, password: $password) {\n    token\n  }\n}\n    "], ["\n    mutation login($username: String!, $password: String!) {\n  login(username: $username, password: $password) {\n    token\n  }\n}\n    "])));
export var LoginComponent = function (props) { return (React.createElement(ApolloReactComponents.Mutation, __assign({ mutation: LoginDocument }, props))); };
export function withLogin(operationOptions) {
    return ApolloReactHoc.withMutation(LoginDocument, __assign({ alias: 'login' }, operationOptions));
}
;
/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      username: // value for 'username'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions) {
    return ApolloReactHooks.useMutation(LoginDocument, baseOptions);
}
export var ProfileDocument = gql(templateObject_2 || (templateObject_2 = __makeTemplateObject(["\n    query profile($id: ID!) {\n  getUserById(id: $id) {\n    id\n    name\n    username\n    email\n    createdAt\n    roles\n  }\n}\n    "], ["\n    query profile($id: ID!) {\n  getUserById(id: $id) {\n    id\n    name\n    username\n    email\n    createdAt\n    roles\n  }\n}\n    "])));
export var ProfileComponent = function (props) { return (React.createElement(ApolloReactComponents.Query, __assign({ query: ProfileDocument }, props))); };
export function withProfile(operationOptions) {
    return ApolloReactHoc.withQuery(ProfileDocument, __assign({ alias: 'profile' }, operationOptions));
}
;
/**
 * __useProfileQuery__
 *
 * To run a query within a React component, call `useProfileQuery` and pass it any options that fit your needs.
 * When your component renders, `useProfileQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useProfileQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useProfileQuery(baseOptions) {
    return ApolloReactHooks.useQuery(ProfileDocument, baseOptions);
}
export function useProfileLazyQuery(baseOptions) {
    return ApolloReactHooks.useLazyQuery(ProfileDocument, baseOptions);
}
export var SignUpDocument = gql(templateObject_3 || (templateObject_3 = __makeTemplateObject(["\n    mutation signUp($email: String!, $username: String!, $name: String, $password: String!) {\n  signUp(username: $username, password: $password, name: $name, email: $email) {\n    token\n  }\n}\n    "], ["\n    mutation signUp($email: String!, $username: String!, $name: String, $password: String!) {\n  signUp(username: $username, password: $password, name: $name, email: $email) {\n    token\n  }\n}\n    "])));
export var SignUpComponent = function (props) { return (React.createElement(ApolloReactComponents.Mutation, __assign({ mutation: SignUpDocument }, props))); };
export function withSignUp(operationOptions) {
    return ApolloReactHoc.withMutation(SignUpDocument, __assign({ alias: 'signUp' }, operationOptions));
}
;
/**
 * __useSignUpMutation__
 *
 * To run a mutation, you first call `useSignUpMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignUpMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signUpMutation, { data, loading, error }] = useSignUpMutation({
 *   variables: {
 *      email: // value for 'email'
 *      username: // value for 'username'
 *      name: // value for 'name'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useSignUpMutation(baseOptions) {
    return ApolloReactHooks.useMutation(SignUpDocument, baseOptions);
}
export var GetWorkoutDocument = gql(templateObject_4 || (templateObject_4 = __makeTemplateObject(["\n    query getWorkout($id: ID!) {\n  getWorkoutById(id: $id) {\n    id\n    name\n    date\n    duration\n    exercises {\n      name\n      sets {\n        repetitions\n        weight\n        type\n      }\n    }\n  }\n}\n    "], ["\n    query getWorkout($id: ID!) {\n  getWorkoutById(id: $id) {\n    id\n    name\n    date\n    duration\n    exercises {\n      name\n      sets {\n        repetitions\n        weight\n        type\n      }\n    }\n  }\n}\n    "])));
export var GetWorkoutComponent = function (props) { return (React.createElement(ApolloReactComponents.Query, __assign({ query: GetWorkoutDocument }, props))); };
export function withGetWorkout(operationOptions) {
    return ApolloReactHoc.withQuery(GetWorkoutDocument, __assign({ alias: 'getWorkout' }, operationOptions));
}
;
/**
 * __useGetWorkoutQuery__
 *
 * To run a query within a React component, call `useGetWorkoutQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetWorkoutQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetWorkoutQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetWorkoutQuery(baseOptions) {
    return ApolloReactHooks.useQuery(GetWorkoutDocument, baseOptions);
}
export function useGetWorkoutLazyQuery(baseOptions) {
    return ApolloReactHooks.useLazyQuery(GetWorkoutDocument, baseOptions);
}
export var RegisterWorkoutDocument = gql(templateObject_5 || (templateObject_5 = __makeTemplateObject(["\n    mutation registerWorkout($name: String!, $userId: ID!, $date: String!, $duration: Float!, $exercises: [ExerciseInput!]) {\n  registerWorkout(name: $name, userId: $userId, date: $date, duration: $duration, exercises: $exercises) {\n    id\n    name\n    userId\n  }\n}\n    "], ["\n    mutation registerWorkout($name: String!, $userId: ID!, $date: String!, $duration: Float!, $exercises: [ExerciseInput!]) {\n  registerWorkout(name: $name, userId: $userId, date: $date, duration: $duration, exercises: $exercises) {\n    id\n    name\n    userId\n  }\n}\n    "])));
export var RegisterWorkoutComponent = function (props) { return (React.createElement(ApolloReactComponents.Mutation, __assign({ mutation: RegisterWorkoutDocument }, props))); };
export function withRegisterWorkout(operationOptions) {
    return ApolloReactHoc.withMutation(RegisterWorkoutDocument, __assign({ alias: 'registerWorkout' }, operationOptions));
}
;
/**
 * __useRegisterWorkoutMutation__
 *
 * To run a mutation, you first call `useRegisterWorkoutMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterWorkoutMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerWorkoutMutation, { data, loading, error }] = useRegisterWorkoutMutation({
 *   variables: {
 *      name: // value for 'name'
 *      userId: // value for 'userId'
 *      date: // value for 'date'
 *      duration: // value for 'duration'
 *      exercises: // value for 'exercises'
 *   },
 * });
 */
export function useRegisterWorkoutMutation(baseOptions) {
    return ApolloReactHooks.useMutation(RegisterWorkoutDocument, baseOptions);
}
export var GetUserExercisesDocument = gql(templateObject_6 || (templateObject_6 = __makeTemplateObject(["\n    query getUserExercises($userId: ID!) {\n  getExercisesByUserId(userId: $userId) {\n    name\n    id\n    sets {\n      repetitions\n      weight\n      type\n    }\n  }\n}\n    "], ["\n    query getUserExercises($userId: ID!) {\n  getExercisesByUserId(userId: $userId) {\n    name\n    id\n    sets {\n      repetitions\n      weight\n      type\n    }\n  }\n}\n    "])));
export var GetUserExercisesComponent = function (props) { return (React.createElement(ApolloReactComponents.Query, __assign({ query: GetUserExercisesDocument }, props))); };
export function withGetUserExercises(operationOptions) {
    return ApolloReactHoc.withQuery(GetUserExercisesDocument, __assign({ alias: 'getUserExercises' }, operationOptions));
}
;
/**
 * __useGetUserExercisesQuery__
 *
 * To run a query within a React component, call `useGetUserExercisesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserExercisesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserExercisesQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useGetUserExercisesQuery(baseOptions) {
    return ApolloReactHooks.useQuery(GetUserExercisesDocument, baseOptions);
}
export function useGetUserExercisesLazyQuery(baseOptions) {
    return ApolloReactHooks.useLazyQuery(GetUserExercisesDocument, baseOptions);
}
export var GetUserWorkoutOverviewDocument = gql(templateObject_7 || (templateObject_7 = __makeTemplateObject(["\n    query getUserWorkoutOverview($userId: ID!) {\n  getWorkoutOverviewStats(userId: $userId) {\n    duration\n    workoutCount\n  }\n}\n    "], ["\n    query getUserWorkoutOverview($userId: ID!) {\n  getWorkoutOverviewStats(userId: $userId) {\n    duration\n    workoutCount\n  }\n}\n    "])));
export var GetUserWorkoutOverviewComponent = function (props) { return (React.createElement(ApolloReactComponents.Query, __assign({ query: GetUserWorkoutOverviewDocument }, props))); };
export function withGetUserWorkoutOverview(operationOptions) {
    return ApolloReactHoc.withQuery(GetUserWorkoutOverviewDocument, __assign({ alias: 'getUserWorkoutOverview' }, operationOptions));
}
;
/**
 * __useGetUserWorkoutOverviewQuery__
 *
 * To run a query within a React component, call `useGetUserWorkoutOverviewQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserWorkoutOverviewQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserWorkoutOverviewQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useGetUserWorkoutOverviewQuery(baseOptions) {
    return ApolloReactHooks.useQuery(GetUserWorkoutOverviewDocument, baseOptions);
}
export function useGetUserWorkoutOverviewLazyQuery(baseOptions) {
    return ApolloReactHooks.useLazyQuery(GetUserWorkoutOverviewDocument, baseOptions);
}
export var GetAllUserWorkoutsDocument = gql(templateObject_8 || (templateObject_8 = __makeTemplateObject(["\n    query getAllUserWorkouts($userId: ID!) {\n  getWorkoutsByUserId(userId: $userId) {\n    id\n    userId\n    name\n    duration\n    createdAt\n  }\n}\n    "], ["\n    query getAllUserWorkouts($userId: ID!) {\n  getWorkoutsByUserId(userId: $userId) {\n    id\n    userId\n    name\n    duration\n    createdAt\n  }\n}\n    "])));
export var GetAllUserWorkoutsComponent = function (props) { return (React.createElement(ApolloReactComponents.Query, __assign({ query: GetAllUserWorkoutsDocument }, props))); };
export function withGetAllUserWorkouts(operationOptions) {
    return ApolloReactHoc.withQuery(GetAllUserWorkoutsDocument, __assign({ alias: 'getAllUserWorkouts' }, operationOptions));
}
;
/**
 * __useGetAllUserWorkoutsQuery__
 *
 * To run a query within a React component, call `useGetAllUserWorkoutsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAllUserWorkoutsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAllUserWorkoutsQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useGetAllUserWorkoutsQuery(baseOptions) {
    return ApolloReactHooks.useQuery(GetAllUserWorkoutsDocument, baseOptions);
}
export function useGetAllUserWorkoutsLazyQuery(baseOptions) {
    return ApolloReactHooks.useLazyQuery(GetAllUserWorkoutsDocument, baseOptions);
}
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8;
//# sourceMappingURL=index.js.map