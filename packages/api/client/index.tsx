/*eslint-disable */
import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as React from 'react';
import * as ApolloReactComponents from '@apollo/react-components';
import * as ApolloReactHoc from '@apollo/react-hoc';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type Maybe<T> = T | null;
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
};



export type Exercise = {
   __typename?: 'Exercise',
  id: Scalars['ID'],
  userId: Scalars['ID'],
  name: Scalars['String'],
  sets?: Maybe<Array<Sets>>,
};

export type ExerciseInput = {
  id: Scalars['ID'],
  name: Scalars['String'],
  sets?: Maybe<Array<SetsInput>>,
  userId: Scalars['ID'],
};

export type Mutation = {
   __typename?: 'Mutation',
  login?: Maybe<Token>,
  registerWorkout?: Maybe<Workout>,
  signUp?: Maybe<Token>,
};


export type MutationLoginArgs = {
  username: Scalars['String'],
  password: Scalars['String']
};


export type MutationRegisterWorkoutArgs = {
  name: Scalars['String'],
  date: Scalars['String'],
  userId: Scalars['ID'],
  duration: Scalars['Float'],
  exercises?: Maybe<Array<ExerciseInput>>
};


export type MutationSignUpArgs = {
  email: Scalars['String'],
  username: Scalars['String'],
  name?: Maybe<Scalars['String']>,
  password: Scalars['String']
};

export type Query = {
   __typename?: 'Query',
  getWorkoutById?: Maybe<Workout>,
  getWorkoutsByUserId: Array<Workout>,
  getWorkoutOverviewStats?: Maybe<WorkoutSummary>,
  getExercisesByUserId: Array<Exercise>,
  getUserById?: Maybe<User>,
  getAllUsers: Array<User>,
};


export type QueryGetWorkoutByIdArgs = {
  id: Scalars['ID']
};


export type QueryGetWorkoutsByUserIdArgs = {
  userId: Scalars['ID']
};


export type QueryGetWorkoutOverviewStatsArgs = {
  userId: Scalars['ID']
};


export type QueryGetExercisesByUserIdArgs = {
  userId: Scalars['ID']
};


export type QueryGetUserByIdArgs = {
  id: Scalars['ID']
};

export enum Role {
  Admin = 'ADMIN',
  User = 'USER'
}

export type Sets = {
   __typename?: 'Sets',
  repetitions: Scalars['Int'],
  weight: Scalars['Float'],
  type: SetType,
};

export type SetsInput = {
  repetitions: Scalars['Int'],
  weight: Scalars['Float'],
  type: SetType,
};

export enum SetType {
  Normal = 'NORMAL',
  Warmup = 'WARMUP',
  Failure = 'FAILURE'
}

export type Token = {
   __typename?: 'Token',
  token: Scalars['String'],
};

export type User = {
   __typename?: 'User',
  id?: Maybe<Scalars['ID']>,
  name?: Maybe<Scalars['String']>,
  username: Scalars['String'],
  password: Scalars['String'],
  email: Scalars['String'],
  createdAt: Scalars['String'],
  roles: Array<Role>,
  workouts?: Maybe<Array<Workout>>,
  exercises?: Maybe<Array<Exercise>>,
};

export type Workout = {
   __typename?: 'Workout',
  id: Scalars['ID'],
  userId: Scalars['ID'],
  name: Scalars['String'],
  date: Scalars['String'],
  createdAt: Scalars['String'],
  exercises?: Maybe<Array<Exercise>>,
  duration: Scalars['Float'],
};

export type WorkoutSummary = {
   __typename?: 'WorkoutSummary',
  workoutCount: Scalars['Int'],
  duration: Scalars['Float'],
};

export type LoginMutationVariables = {
  username: Scalars['String'],
  password: Scalars['String']
};


export type LoginMutation = (
  { __typename?: 'Mutation' }
  & { login: Maybe<(
    { __typename?: 'Token' }
    & Pick<Token, 'token'>
  )> }
);

export type ProfileQueryVariables = {
  id: Scalars['ID']
};


export type ProfileQuery = (
  { __typename?: 'Query' }
  & { getUserById: Maybe<(
    { __typename?: 'User' }
    & Pick<User, 'id' | 'name' | 'username' | 'email' | 'createdAt' | 'roles'>
  )> }
);

export type SignUpMutationVariables = {
  email: Scalars['String'],
  username: Scalars['String'],
  name?: Maybe<Scalars['String']>,
  password: Scalars['String']
};


export type SignUpMutation = (
  { __typename?: 'Mutation' }
  & { signUp: Maybe<(
    { __typename?: 'Token' }
    & Pick<Token, 'token'>
  )> }
);

export type GetWorkoutQueryVariables = {
  id: Scalars['ID']
};


export type GetWorkoutQuery = (
  { __typename?: 'Query' }
  & { getWorkoutById: Maybe<(
    { __typename?: 'Workout' }
    & Pick<Workout, 'id' | 'name' | 'date' | 'duration'>
    & { exercises: Maybe<Array<(
      { __typename?: 'Exercise' }
      & Pick<Exercise, 'id' | 'name'>
      & { sets: Maybe<Array<(
        { __typename?: 'Sets' }
        & Pick<Sets, 'repetitions' | 'weight' | 'type'>
      )>> }
    )>> }
  )> }
);

export type RegisterWorkoutMutationVariables = {
  name: Scalars['String'],
  userId: Scalars['ID'],
  date: Scalars['String'],
  duration: Scalars['Float'],
  exercises?: Maybe<Array<ExerciseInput>>
};


export type RegisterWorkoutMutation = (
  { __typename?: 'Mutation' }
  & { registerWorkout: Maybe<(
    { __typename?: 'Workout' }
    & Pick<Workout, 'id' | 'name' | 'userId'>
  )> }
);

export type GetUserExercisesQueryVariables = {
  userId: Scalars['ID']
};


export type GetUserExercisesQuery = (
  { __typename?: 'Query' }
  & { getExercisesByUserId: Array<(
    { __typename?: 'Exercise' }
    & Pick<Exercise, 'name' | 'id'>
    & { sets: Maybe<Array<(
      { __typename?: 'Sets' }
      & Pick<Sets, 'repetitions' | 'weight' | 'type'>
    )>> }
  )> }
);

export type GetUserWorkoutOverviewQueryVariables = {
  userId: Scalars['ID']
};


export type GetUserWorkoutOverviewQuery = (
  { __typename?: 'Query' }
  & { getWorkoutOverviewStats: Maybe<(
    { __typename?: 'WorkoutSummary' }
    & Pick<WorkoutSummary, 'duration' | 'workoutCount'>
  )> }
);

export type GetAllUserWorkoutsQueryVariables = {
  userId: Scalars['ID']
};


export type GetAllUserWorkoutsQuery = (
  { __typename?: 'Query' }
  & { getWorkoutsByUserId: Array<(
    { __typename?: 'Workout' }
    & Pick<Workout, 'id' | 'userId' | 'name' | 'duration' | 'createdAt'>
  )> }
);


export const LoginDocument = gql`
    mutation login($username: String!, $password: String!) {
  login(username: $username, password: $password) {
    token
  }
}
    `;
export type LoginMutationFn = ApolloReactCommon.MutationFunction<LoginMutation, LoginMutationVariables>;
export type LoginComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<LoginMutation, LoginMutationVariables>, 'mutation'>;

    export const LoginComponent = (props: LoginComponentProps) => (
      <ApolloReactComponents.Mutation<LoginMutation, LoginMutationVariables> mutation={LoginDocument} {...props} />
    );
    
export type LoginProps<TChildProps = {}> = ApolloReactHoc.MutateProps<LoginMutation, LoginMutationVariables> & TChildProps;
export function withLogin<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  LoginMutation,
  LoginMutationVariables,
  LoginProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, LoginMutation, LoginMutationVariables, LoginProps<TChildProps>>(LoginDocument, {
      alias: 'login',
      ...operationOptions
    });
};

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      username: // value for 'username'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        return ApolloReactHooks.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, baseOptions);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = ApolloReactCommon.MutationResult<LoginMutation>;
export type LoginMutationOptions = ApolloReactCommon.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const ProfileDocument = gql`
    query profile($id: ID!) {
  getUserById(id: $id) {
    id
    name
    username
    email
    createdAt
    roles
  }
}
    `;
export type ProfileComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ProfileQuery, ProfileQueryVariables>, 'query'> & ({ variables: ProfileQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const ProfileComponent = (props: ProfileComponentProps) => (
      <ApolloReactComponents.Query<ProfileQuery, ProfileQueryVariables> query={ProfileDocument} {...props} />
    );
    
export type ProfileProps<TChildProps = {}> = ApolloReactHoc.DataProps<ProfileQuery, ProfileQueryVariables> & TChildProps;
export function withProfile<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  ProfileQuery,
  ProfileQueryVariables,
  ProfileProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, ProfileQuery, ProfileQueryVariables, ProfileProps<TChildProps>>(ProfileDocument, {
      alias: 'profile',
      ...operationOptions
    });
};

/**
 * __useProfileQuery__
 *
 * To run a query within a React component, call `useProfileQuery` and pass it any options that fit your needs.
 * When your component renders, `useProfileQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useProfileQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useProfileQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ProfileQuery, ProfileQueryVariables>) {
        return ApolloReactHooks.useQuery<ProfileQuery, ProfileQueryVariables>(ProfileDocument, baseOptions);
      }
export function useProfileLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ProfileQuery, ProfileQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<ProfileQuery, ProfileQueryVariables>(ProfileDocument, baseOptions);
        }
export type ProfileQueryHookResult = ReturnType<typeof useProfileQuery>;
export type ProfileLazyQueryHookResult = ReturnType<typeof useProfileLazyQuery>;
export type ProfileQueryResult = ApolloReactCommon.QueryResult<ProfileQuery, ProfileQueryVariables>;
export const SignUpDocument = gql`
    mutation signUp($email: String!, $username: String!, $name: String, $password: String!) {
  signUp(username: $username, password: $password, name: $name, email: $email) {
    token
  }
}
    `;
export type SignUpMutationFn = ApolloReactCommon.MutationFunction<SignUpMutation, SignUpMutationVariables>;
export type SignUpComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<SignUpMutation, SignUpMutationVariables>, 'mutation'>;

    export const SignUpComponent = (props: SignUpComponentProps) => (
      <ApolloReactComponents.Mutation<SignUpMutation, SignUpMutationVariables> mutation={SignUpDocument} {...props} />
    );
    
export type SignUpProps<TChildProps = {}> = ApolloReactHoc.MutateProps<SignUpMutation, SignUpMutationVariables> & TChildProps;
export function withSignUp<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  SignUpMutation,
  SignUpMutationVariables,
  SignUpProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, SignUpMutation, SignUpMutationVariables, SignUpProps<TChildProps>>(SignUpDocument, {
      alias: 'signUp',
      ...operationOptions
    });
};

/**
 * __useSignUpMutation__
 *
 * To run a mutation, you first call `useSignUpMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignUpMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signUpMutation, { data, loading, error }] = useSignUpMutation({
 *   variables: {
 *      email: // value for 'email'
 *      username: // value for 'username'
 *      name: // value for 'name'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useSignUpMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<SignUpMutation, SignUpMutationVariables>) {
        return ApolloReactHooks.useMutation<SignUpMutation, SignUpMutationVariables>(SignUpDocument, baseOptions);
      }
export type SignUpMutationHookResult = ReturnType<typeof useSignUpMutation>;
export type SignUpMutationResult = ApolloReactCommon.MutationResult<SignUpMutation>;
export type SignUpMutationOptions = ApolloReactCommon.BaseMutationOptions<SignUpMutation, SignUpMutationVariables>;
export const GetWorkoutDocument = gql`
    query getWorkout($id: ID!) {
  getWorkoutById(id: $id) {
    id
    name
    date
    duration
    exercises {
      id
      name
      sets {
        repetitions
        weight
        type
      }
    }
  }
}
    `;
export type GetWorkoutComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetWorkoutQuery, GetWorkoutQueryVariables>, 'query'> & ({ variables: GetWorkoutQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetWorkoutComponent = (props: GetWorkoutComponentProps) => (
      <ApolloReactComponents.Query<GetWorkoutQuery, GetWorkoutQueryVariables> query={GetWorkoutDocument} {...props} />
    );
    
export type GetWorkoutProps<TChildProps = {}> = ApolloReactHoc.DataProps<GetWorkoutQuery, GetWorkoutQueryVariables> & TChildProps;
export function withGetWorkout<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  GetWorkoutQuery,
  GetWorkoutQueryVariables,
  GetWorkoutProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, GetWorkoutQuery, GetWorkoutQueryVariables, GetWorkoutProps<TChildProps>>(GetWorkoutDocument, {
      alias: 'getWorkout',
      ...operationOptions
    });
};

/**
 * __useGetWorkoutQuery__
 *
 * To run a query within a React component, call `useGetWorkoutQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetWorkoutQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetWorkoutQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetWorkoutQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetWorkoutQuery, GetWorkoutQueryVariables>) {
        return ApolloReactHooks.useQuery<GetWorkoutQuery, GetWorkoutQueryVariables>(GetWorkoutDocument, baseOptions);
      }
export function useGetWorkoutLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetWorkoutQuery, GetWorkoutQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetWorkoutQuery, GetWorkoutQueryVariables>(GetWorkoutDocument, baseOptions);
        }
export type GetWorkoutQueryHookResult = ReturnType<typeof useGetWorkoutQuery>;
export type GetWorkoutLazyQueryHookResult = ReturnType<typeof useGetWorkoutLazyQuery>;
export type GetWorkoutQueryResult = ApolloReactCommon.QueryResult<GetWorkoutQuery, GetWorkoutQueryVariables>;
export const RegisterWorkoutDocument = gql`
    mutation registerWorkout($name: String!, $userId: ID!, $date: String!, $duration: Float!, $exercises: [ExerciseInput!]) {
  registerWorkout(name: $name, userId: $userId, date: $date, duration: $duration, exercises: $exercises) {
    id
    name
    userId
  }
}
    `;
export type RegisterWorkoutMutationFn = ApolloReactCommon.MutationFunction<RegisterWorkoutMutation, RegisterWorkoutMutationVariables>;
export type RegisterWorkoutComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<RegisterWorkoutMutation, RegisterWorkoutMutationVariables>, 'mutation'>;

    export const RegisterWorkoutComponent = (props: RegisterWorkoutComponentProps) => (
      <ApolloReactComponents.Mutation<RegisterWorkoutMutation, RegisterWorkoutMutationVariables> mutation={RegisterWorkoutDocument} {...props} />
    );
    
export type RegisterWorkoutProps<TChildProps = {}> = ApolloReactHoc.MutateProps<RegisterWorkoutMutation, RegisterWorkoutMutationVariables> & TChildProps;
export function withRegisterWorkout<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  RegisterWorkoutMutation,
  RegisterWorkoutMutationVariables,
  RegisterWorkoutProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, RegisterWorkoutMutation, RegisterWorkoutMutationVariables, RegisterWorkoutProps<TChildProps>>(RegisterWorkoutDocument, {
      alias: 'registerWorkout',
      ...operationOptions
    });
};

/**
 * __useRegisterWorkoutMutation__
 *
 * To run a mutation, you first call `useRegisterWorkoutMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterWorkoutMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerWorkoutMutation, { data, loading, error }] = useRegisterWorkoutMutation({
 *   variables: {
 *      name: // value for 'name'
 *      userId: // value for 'userId'
 *      date: // value for 'date'
 *      duration: // value for 'duration'
 *      exercises: // value for 'exercises'
 *   },
 * });
 */
export function useRegisterWorkoutMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<RegisterWorkoutMutation, RegisterWorkoutMutationVariables>) {
        return ApolloReactHooks.useMutation<RegisterWorkoutMutation, RegisterWorkoutMutationVariables>(RegisterWorkoutDocument, baseOptions);
      }
export type RegisterWorkoutMutationHookResult = ReturnType<typeof useRegisterWorkoutMutation>;
export type RegisterWorkoutMutationResult = ApolloReactCommon.MutationResult<RegisterWorkoutMutation>;
export type RegisterWorkoutMutationOptions = ApolloReactCommon.BaseMutationOptions<RegisterWorkoutMutation, RegisterWorkoutMutationVariables>;
export const GetUserExercisesDocument = gql`
    query getUserExercises($userId: ID!) {
  getExercisesByUserId(userId: $userId) {
    name
    id
    sets {
      repetitions
      weight
      type
    }
  }
}
    `;
export type GetUserExercisesComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetUserExercisesQuery, GetUserExercisesQueryVariables>, 'query'> & ({ variables: GetUserExercisesQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetUserExercisesComponent = (props: GetUserExercisesComponentProps) => (
      <ApolloReactComponents.Query<GetUserExercisesQuery, GetUserExercisesQueryVariables> query={GetUserExercisesDocument} {...props} />
    );
    
export type GetUserExercisesProps<TChildProps = {}> = ApolloReactHoc.DataProps<GetUserExercisesQuery, GetUserExercisesQueryVariables> & TChildProps;
export function withGetUserExercises<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  GetUserExercisesQuery,
  GetUserExercisesQueryVariables,
  GetUserExercisesProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, GetUserExercisesQuery, GetUserExercisesQueryVariables, GetUserExercisesProps<TChildProps>>(GetUserExercisesDocument, {
      alias: 'getUserExercises',
      ...operationOptions
    });
};

/**
 * __useGetUserExercisesQuery__
 *
 * To run a query within a React component, call `useGetUserExercisesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserExercisesQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserExercisesQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useGetUserExercisesQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetUserExercisesQuery, GetUserExercisesQueryVariables>) {
        return ApolloReactHooks.useQuery<GetUserExercisesQuery, GetUserExercisesQueryVariables>(GetUserExercisesDocument, baseOptions);
      }
export function useGetUserExercisesLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetUserExercisesQuery, GetUserExercisesQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetUserExercisesQuery, GetUserExercisesQueryVariables>(GetUserExercisesDocument, baseOptions);
        }
export type GetUserExercisesQueryHookResult = ReturnType<typeof useGetUserExercisesQuery>;
export type GetUserExercisesLazyQueryHookResult = ReturnType<typeof useGetUserExercisesLazyQuery>;
export type GetUserExercisesQueryResult = ApolloReactCommon.QueryResult<GetUserExercisesQuery, GetUserExercisesQueryVariables>;
export const GetUserWorkoutOverviewDocument = gql`
    query getUserWorkoutOverview($userId: ID!) {
  getWorkoutOverviewStats(userId: $userId) {
    duration
    workoutCount
  }
}
    `;
export type GetUserWorkoutOverviewComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables>, 'query'> & ({ variables: GetUserWorkoutOverviewQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetUserWorkoutOverviewComponent = (props: GetUserWorkoutOverviewComponentProps) => (
      <ApolloReactComponents.Query<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables> query={GetUserWorkoutOverviewDocument} {...props} />
    );
    
export type GetUserWorkoutOverviewProps<TChildProps = {}> = ApolloReactHoc.DataProps<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables> & TChildProps;
export function withGetUserWorkoutOverview<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  GetUserWorkoutOverviewQuery,
  GetUserWorkoutOverviewQueryVariables,
  GetUserWorkoutOverviewProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables, GetUserWorkoutOverviewProps<TChildProps>>(GetUserWorkoutOverviewDocument, {
      alias: 'getUserWorkoutOverview',
      ...operationOptions
    });
};

/**
 * __useGetUserWorkoutOverviewQuery__
 *
 * To run a query within a React component, call `useGetUserWorkoutOverviewQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserWorkoutOverviewQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserWorkoutOverviewQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useGetUserWorkoutOverviewQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables>) {
        return ApolloReactHooks.useQuery<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables>(GetUserWorkoutOverviewDocument, baseOptions);
      }
export function useGetUserWorkoutOverviewLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables>(GetUserWorkoutOverviewDocument, baseOptions);
        }
export type GetUserWorkoutOverviewQueryHookResult = ReturnType<typeof useGetUserWorkoutOverviewQuery>;
export type GetUserWorkoutOverviewLazyQueryHookResult = ReturnType<typeof useGetUserWorkoutOverviewLazyQuery>;
export type GetUserWorkoutOverviewQueryResult = ApolloReactCommon.QueryResult<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables>;
export const GetAllUserWorkoutsDocument = gql`
    query getAllUserWorkouts($userId: ID!) {
  getWorkoutsByUserId(userId: $userId) {
    id
    userId
    name
    duration
    createdAt
  }
}
    `;
export type GetAllUserWorkoutsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables>, 'query'> & ({ variables: GetAllUserWorkoutsQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetAllUserWorkoutsComponent = (props: GetAllUserWorkoutsComponentProps) => (
      <ApolloReactComponents.Query<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables> query={GetAllUserWorkoutsDocument} {...props} />
    );
    
export type GetAllUserWorkoutsProps<TChildProps = {}> = ApolloReactHoc.DataProps<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables> & TChildProps;
export function withGetAllUserWorkouts<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  GetAllUserWorkoutsQuery,
  GetAllUserWorkoutsQueryVariables,
  GetAllUserWorkoutsProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables, GetAllUserWorkoutsProps<TChildProps>>(GetAllUserWorkoutsDocument, {
      alias: 'getAllUserWorkouts',
      ...operationOptions
    });
};

/**
 * __useGetAllUserWorkoutsQuery__
 *
 * To run a query within a React component, call `useGetAllUserWorkoutsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAllUserWorkoutsQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAllUserWorkoutsQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useGetAllUserWorkoutsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables>(GetAllUserWorkoutsDocument, baseOptions);
      }
export function useGetAllUserWorkoutsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables>(GetAllUserWorkoutsDocument, baseOptions);
        }
export type GetAllUserWorkoutsQueryHookResult = ReturnType<typeof useGetAllUserWorkoutsQuery>;
export type GetAllUserWorkoutsLazyQueryHookResult = ReturnType<typeof useGetAllUserWorkoutsLazyQuery>;
export type GetAllUserWorkoutsQueryResult = ApolloReactCommon.QueryResult<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables>;