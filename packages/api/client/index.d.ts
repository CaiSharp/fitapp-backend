import * as ApolloReactCommon from '@apollo/react-common';
import * as React from 'react';
import * as ApolloReactComponents from '@apollo/react-components';
import * as ApolloReactHoc from '@apollo/react-hoc';
import * as ApolloReactHooks from '@apollo/react-hooks';
export declare type Maybe<T> = T | null;
export declare type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export declare type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
};
export declare type Exercise = {
    __typename?: 'Exercise';
    id: Scalars['ID'];
    userId: Scalars['ID'];
    name: Scalars['String'];
    sets?: Maybe<Array<Sets>>;
};
export declare type ExerciseInput = {
    id: Scalars['ID'];
    name: Scalars['String'];
    sets?: Maybe<Array<SetsInput>>;
    userId: Scalars['ID'];
};
export declare type Mutation = {
    __typename?: 'Mutation';
    login?: Maybe<Token>;
    registerWorkout?: Maybe<Workout>;
    signUp?: Maybe<Token>;
};
export declare type MutationLoginArgs = {
    username: Scalars['String'];
    password: Scalars['String'];
};
export declare type MutationRegisterWorkoutArgs = {
    name: Scalars['String'];
    date: Scalars['String'];
    userId: Scalars['ID'];
    duration: Scalars['Float'];
    exercises?: Maybe<Array<ExerciseInput>>;
};
export declare type MutationSignUpArgs = {
    email: Scalars['String'];
    username: Scalars['String'];
    name?: Maybe<Scalars['String']>;
    password: Scalars['String'];
};
export declare type Query = {
    __typename?: 'Query';
    getWorkoutById?: Maybe<Workout>;
    getWorkoutsByUserId: Array<Workout>;
    getWorkoutOverviewStats?: Maybe<WorkoutSummary>;
    getExercisesByUserId: Array<Exercise>;
    getUserById?: Maybe<User>;
    getAllUsers: Array<User>;
};
export declare type QueryGetWorkoutByIdArgs = {
    id: Scalars['ID'];
};
export declare type QueryGetWorkoutsByUserIdArgs = {
    userId: Scalars['ID'];
};
export declare type QueryGetWorkoutOverviewStatsArgs = {
    userId: Scalars['ID'];
};
export declare type QueryGetExercisesByUserIdArgs = {
    userId: Scalars['ID'];
};
export declare type QueryGetUserByIdArgs = {
    id: Scalars['ID'];
};
export declare enum Role {
    Admin = "ADMIN",
    User = "USER"
}
export declare type Sets = {
    __typename?: 'Sets';
    repetitions: Scalars['Int'];
    weight: Scalars['Float'];
    type: SetType;
};
export declare type SetsInput = {
    repetitions: Scalars['Int'];
    weight: Scalars['Float'];
    type: SetType;
};
export declare enum SetType {
    Normal = "NORMAL",
    Warmup = "WARMUP",
    Failure = "FAILURE"
}
export declare type Token = {
    __typename?: 'Token';
    token: Scalars['String'];
};
export declare type User = {
    __typename?: 'User';
    id?: Maybe<Scalars['ID']>;
    name?: Maybe<Scalars['String']>;
    username: Scalars['String'];
    password: Scalars['String'];
    email: Scalars['String'];
    createdAt: Scalars['String'];
    roles: Array<Role>;
    workouts?: Maybe<Array<Workout>>;
    exercises?: Maybe<Array<Exercise>>;
};
export declare type Workout = {
    __typename?: 'Workout';
    id: Scalars['ID'];
    userId: Scalars['ID'];
    name: Scalars['String'];
    date: Scalars['String'];
    createdAt: Scalars['String'];
    exercises?: Maybe<Array<Exercise>>;
    duration: Scalars['Float'];
};
export declare type WorkoutSummary = {
    __typename?: 'WorkoutSummary';
    workoutCount: Scalars['Int'];
    duration: Scalars['Float'];
};
export declare type LoginMutationVariables = {
    username: Scalars['String'];
    password: Scalars['String'];
};
export declare type LoginMutation = ({
    __typename?: 'Mutation';
} & {
    login: Maybe<({
        __typename?: 'Token';
    } & Pick<Token, 'token'>)>;
});
export declare type ProfileQueryVariables = {
    id: Scalars['ID'];
};
export declare type ProfileQuery = ({
    __typename?: 'Query';
} & {
    getUserById: Maybe<({
        __typename?: 'User';
    } & Pick<User, 'id' | 'name' | 'username' | 'email' | 'createdAt' | 'roles'>)>;
});
export declare type SignUpMutationVariables = {
    email: Scalars['String'];
    username: Scalars['String'];
    name?: Maybe<Scalars['String']>;
    password: Scalars['String'];
};
export declare type SignUpMutation = ({
    __typename?: 'Mutation';
} & {
    signUp: Maybe<({
        __typename?: 'Token';
    } & Pick<Token, 'token'>)>;
});
export declare type GetWorkoutQueryVariables = {
    id: Scalars['ID'];
};
export declare type GetWorkoutQuery = ({
    __typename?: 'Query';
} & {
    getWorkoutById: Maybe<({
        __typename?: 'Workout';
    } & Pick<Workout, 'id' | 'name' | 'date' | 'duration'> & {
        exercises: Maybe<Array<({
            __typename?: 'Exercise';
        } & Pick<Exercise, 'name'> & {
            sets: Maybe<Array<({
                __typename?: 'Sets';
            } & Pick<Sets, 'repetitions' | 'weight' | 'type'>)>>;
        })>>;
    })>;
});
export declare type RegisterWorkoutMutationVariables = {
    name: Scalars['String'];
    userId: Scalars['ID'];
    date: Scalars['String'];
    duration: Scalars['Float'];
    exercises?: Maybe<Array<ExerciseInput>>;
};
export declare type RegisterWorkoutMutation = ({
    __typename?: 'Mutation';
} & {
    registerWorkout: Maybe<({
        __typename?: 'Workout';
    } & Pick<Workout, 'id' | 'name' | 'userId'>)>;
});
export declare type GetUserExercisesQueryVariables = {
    userId: Scalars['ID'];
};
export declare type GetUserExercisesQuery = ({
    __typename?: 'Query';
} & {
    getExercisesByUserId: Array<({
        __typename?: 'Exercise';
    } & Pick<Exercise, 'name' | 'id'> & {
        sets: Maybe<Array<({
            __typename?: 'Sets';
        } & Pick<Sets, 'repetitions' | 'weight' | 'type'>)>>;
    })>;
});
export declare type GetUserWorkoutOverviewQueryVariables = {
    userId: Scalars['ID'];
};
export declare type GetUserWorkoutOverviewQuery = ({
    __typename?: 'Query';
} & {
    getWorkoutOverviewStats: Maybe<({
        __typename?: 'WorkoutSummary';
    } & Pick<WorkoutSummary, 'duration' | 'workoutCount'>)>;
});
export declare type GetAllUserWorkoutsQueryVariables = {
    userId: Scalars['ID'];
};
export declare type GetAllUserWorkoutsQuery = ({
    __typename?: 'Query';
} & {
    getWorkoutsByUserId: Array<({
        __typename?: 'Workout';
    } & Pick<Workout, 'id' | 'userId' | 'name' | 'duration' | 'createdAt'>)>;
});
export declare const LoginDocument: import("graphql").DocumentNode;
export declare type LoginMutationFn = ApolloReactCommon.MutationFunction<LoginMutation, LoginMutationVariables>;
export declare type LoginComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<LoginMutation, LoginMutationVariables>, 'mutation'>;
export declare const LoginComponent: (props: Pick<ApolloReactComponents.MutationComponentOptions<LoginMutation, LoginMutationVariables>, "client" | "update" | "children" | "context" | "variables" | "optimisticResponse" | "refetchQueries" | "awaitRefetchQueries" | "errorPolicy" | "fetchPolicy" | "onCompleted" | "onError" | "notifyOnNetworkStatusChange" | "ignoreResults">) => JSX.Element;
export declare type LoginProps<TChildProps = {}> = ApolloReactHoc.MutateProps<LoginMutation, LoginMutationVariables> & TChildProps;
export declare function withLogin<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<TProps, LoginMutation, LoginMutationVariables, LoginProps<TChildProps>>): (WrappedComponent: React.ComponentType<TProps & ApolloReactHoc.MutateProps<LoginMutation, LoginMutationVariables> & TChildProps>) => React.ComponentClass<TProps, any>;
/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      username: // value for 'username'
 *      password: // value for 'password'
 *   },
 * });
 */
export declare function useLoginMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LoginMutation, LoginMutationVariables>): ApolloReactHooks.MutationTuple<LoginMutation, LoginMutationVariables>;
export declare type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export declare type LoginMutationResult = ApolloReactCommon.MutationResult<LoginMutation>;
export declare type LoginMutationOptions = ApolloReactCommon.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export declare const ProfileDocument: import("graphql").DocumentNode;
export declare type ProfileComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ProfileQuery, ProfileQueryVariables>, 'query'> & ({
    variables: ProfileQueryVariables;
    skip?: boolean;
} | {
    skip: boolean;
});
export declare const ProfileComponent: (props: ProfileComponentProps) => JSX.Element;
export declare type ProfileProps<TChildProps = {}> = ApolloReactHoc.DataProps<ProfileQuery, ProfileQueryVariables> & TChildProps;
export declare function withProfile<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<TProps, ProfileQuery, ProfileQueryVariables, ProfileProps<TChildProps>>): (WrappedComponent: React.ComponentType<TProps & ApolloReactHoc.DataProps<ProfileQuery, ProfileQueryVariables> & TChildProps>) => React.ComponentClass<TProps, any>;
/**
 * __useProfileQuery__
 *
 * To run a query within a React component, call `useProfileQuery` and pass it any options that fit your needs.
 * When your component renders, `useProfileQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useProfileQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export declare function useProfileQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ProfileQuery, ProfileQueryVariables>): ApolloReactCommon.QueryResult<ProfileQuery, ProfileQueryVariables>;
export declare function useProfileLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ProfileQuery, ProfileQueryVariables>): [(options?: ApolloReactHooks.QueryLazyOptions<ProfileQueryVariables>) => void, ApolloReactCommon.QueryResult<ProfileQuery, ProfileQueryVariables>];
export declare type ProfileQueryHookResult = ReturnType<typeof useProfileQuery>;
export declare type ProfileLazyQueryHookResult = ReturnType<typeof useProfileLazyQuery>;
export declare type ProfileQueryResult = ApolloReactCommon.QueryResult<ProfileQuery, ProfileQueryVariables>;
export declare const SignUpDocument: import("graphql").DocumentNode;
export declare type SignUpMutationFn = ApolloReactCommon.MutationFunction<SignUpMutation, SignUpMutationVariables>;
export declare type SignUpComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<SignUpMutation, SignUpMutationVariables>, 'mutation'>;
export declare const SignUpComponent: (props: Pick<ApolloReactComponents.MutationComponentOptions<SignUpMutation, SignUpMutationVariables>, "client" | "update" | "children" | "context" | "variables" | "optimisticResponse" | "refetchQueries" | "awaitRefetchQueries" | "errorPolicy" | "fetchPolicy" | "onCompleted" | "onError" | "notifyOnNetworkStatusChange" | "ignoreResults">) => JSX.Element;
export declare type SignUpProps<TChildProps = {}> = ApolloReactHoc.MutateProps<SignUpMutation, SignUpMutationVariables> & TChildProps;
export declare function withSignUp<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<TProps, SignUpMutation, SignUpMutationVariables, SignUpProps<TChildProps>>): (WrappedComponent: React.ComponentType<TProps & ApolloReactHoc.MutateProps<SignUpMutation, SignUpMutationVariables> & TChildProps>) => React.ComponentClass<TProps, any>;
/**
 * __useSignUpMutation__
 *
 * To run a mutation, you first call `useSignUpMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignUpMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signUpMutation, { data, loading, error }] = useSignUpMutation({
 *   variables: {
 *      email: // value for 'email'
 *      username: // value for 'username'
 *      name: // value for 'name'
 *      password: // value for 'password'
 *   },
 * });
 */
export declare function useSignUpMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<SignUpMutation, SignUpMutationVariables>): ApolloReactHooks.MutationTuple<SignUpMutation, SignUpMutationVariables>;
export declare type SignUpMutationHookResult = ReturnType<typeof useSignUpMutation>;
export declare type SignUpMutationResult = ApolloReactCommon.MutationResult<SignUpMutation>;
export declare type SignUpMutationOptions = ApolloReactCommon.BaseMutationOptions<SignUpMutation, SignUpMutationVariables>;
export declare const GetWorkoutDocument: import("graphql").DocumentNode;
export declare type GetWorkoutComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetWorkoutQuery, GetWorkoutQueryVariables>, 'query'> & ({
    variables: GetWorkoutQueryVariables;
    skip?: boolean;
} | {
    skip: boolean;
});
export declare const GetWorkoutComponent: (props: GetWorkoutComponentProps) => JSX.Element;
export declare type GetWorkoutProps<TChildProps = {}> = ApolloReactHoc.DataProps<GetWorkoutQuery, GetWorkoutQueryVariables> & TChildProps;
export declare function withGetWorkout<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<TProps, GetWorkoutQuery, GetWorkoutQueryVariables, GetWorkoutProps<TChildProps>>): (WrappedComponent: React.ComponentType<TProps & ApolloReactHoc.DataProps<GetWorkoutQuery, GetWorkoutQueryVariables> & TChildProps>) => React.ComponentClass<TProps, any>;
/**
 * __useGetWorkoutQuery__
 *
 * To run a query within a React component, call `useGetWorkoutQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetWorkoutQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetWorkoutQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export declare function useGetWorkoutQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetWorkoutQuery, GetWorkoutQueryVariables>): ApolloReactCommon.QueryResult<GetWorkoutQuery, GetWorkoutQueryVariables>;
export declare function useGetWorkoutLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetWorkoutQuery, GetWorkoutQueryVariables>): [(options?: ApolloReactHooks.QueryLazyOptions<GetWorkoutQueryVariables>) => void, ApolloReactCommon.QueryResult<GetWorkoutQuery, GetWorkoutQueryVariables>];
export declare type GetWorkoutQueryHookResult = ReturnType<typeof useGetWorkoutQuery>;
export declare type GetWorkoutLazyQueryHookResult = ReturnType<typeof useGetWorkoutLazyQuery>;
export declare type GetWorkoutQueryResult = ApolloReactCommon.QueryResult<GetWorkoutQuery, GetWorkoutQueryVariables>;
export declare const RegisterWorkoutDocument: import("graphql").DocumentNode;
export declare type RegisterWorkoutMutationFn = ApolloReactCommon.MutationFunction<RegisterWorkoutMutation, RegisterWorkoutMutationVariables>;
export declare type RegisterWorkoutComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<RegisterWorkoutMutation, RegisterWorkoutMutationVariables>, 'mutation'>;
export declare const RegisterWorkoutComponent: (props: Pick<ApolloReactComponents.MutationComponentOptions<RegisterWorkoutMutation, RegisterWorkoutMutationVariables>, "client" | "update" | "children" | "context" | "variables" | "optimisticResponse" | "refetchQueries" | "awaitRefetchQueries" | "errorPolicy" | "fetchPolicy" | "onCompleted" | "onError" | "notifyOnNetworkStatusChange" | "ignoreResults">) => JSX.Element;
export declare type RegisterWorkoutProps<TChildProps = {}> = ApolloReactHoc.MutateProps<RegisterWorkoutMutation, RegisterWorkoutMutationVariables> & TChildProps;
export declare function withRegisterWorkout<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<TProps, RegisterWorkoutMutation, RegisterWorkoutMutationVariables, RegisterWorkoutProps<TChildProps>>): (WrappedComponent: React.ComponentType<TProps & ApolloReactHoc.MutateProps<RegisterWorkoutMutation, RegisterWorkoutMutationVariables> & TChildProps>) => React.ComponentClass<TProps, any>;
/**
 * __useRegisterWorkoutMutation__
 *
 * To run a mutation, you first call `useRegisterWorkoutMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterWorkoutMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerWorkoutMutation, { data, loading, error }] = useRegisterWorkoutMutation({
 *   variables: {
 *      name: // value for 'name'
 *      userId: // value for 'userId'
 *      date: // value for 'date'
 *      duration: // value for 'duration'
 *      exercises: // value for 'exercises'
 *   },
 * });
 */
export declare function useRegisterWorkoutMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<RegisterWorkoutMutation, RegisterWorkoutMutationVariables>): ApolloReactHooks.MutationTuple<RegisterWorkoutMutation, RegisterWorkoutMutationVariables>;
export declare type RegisterWorkoutMutationHookResult = ReturnType<typeof useRegisterWorkoutMutation>;
export declare type RegisterWorkoutMutationResult = ApolloReactCommon.MutationResult<RegisterWorkoutMutation>;
export declare type RegisterWorkoutMutationOptions = ApolloReactCommon.BaseMutationOptions<RegisterWorkoutMutation, RegisterWorkoutMutationVariables>;
export declare const GetUserExercisesDocument: import("graphql").DocumentNode;
export declare type GetUserExercisesComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetUserExercisesQuery, GetUserExercisesQueryVariables>, 'query'> & ({
    variables: GetUserExercisesQueryVariables;
    skip?: boolean;
} | {
    skip: boolean;
});
export declare const GetUserExercisesComponent: (props: GetUserExercisesComponentProps) => JSX.Element;
export declare type GetUserExercisesProps<TChildProps = {}> = ApolloReactHoc.DataProps<GetUserExercisesQuery, GetUserExercisesQueryVariables> & TChildProps;
export declare function withGetUserExercises<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<TProps, GetUserExercisesQuery, GetUserExercisesQueryVariables, GetUserExercisesProps<TChildProps>>): (WrappedComponent: React.ComponentType<TProps & ApolloReactHoc.DataProps<GetUserExercisesQuery, GetUserExercisesQueryVariables> & TChildProps>) => React.ComponentClass<TProps, any>;
/**
 * __useGetUserExercisesQuery__
 *
 * To run a query within a React component, call `useGetUserExercisesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserExercisesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserExercisesQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export declare function useGetUserExercisesQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetUserExercisesQuery, GetUserExercisesQueryVariables>): ApolloReactCommon.QueryResult<GetUserExercisesQuery, GetUserExercisesQueryVariables>;
export declare function useGetUserExercisesLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetUserExercisesQuery, GetUserExercisesQueryVariables>): [(options?: ApolloReactHooks.QueryLazyOptions<GetUserExercisesQueryVariables>) => void, ApolloReactCommon.QueryResult<GetUserExercisesQuery, GetUserExercisesQueryVariables>];
export declare type GetUserExercisesQueryHookResult = ReturnType<typeof useGetUserExercisesQuery>;
export declare type GetUserExercisesLazyQueryHookResult = ReturnType<typeof useGetUserExercisesLazyQuery>;
export declare type GetUserExercisesQueryResult = ApolloReactCommon.QueryResult<GetUserExercisesQuery, GetUserExercisesQueryVariables>;
export declare const GetUserWorkoutOverviewDocument: import("graphql").DocumentNode;
export declare type GetUserWorkoutOverviewComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables>, 'query'> & ({
    variables: GetUserWorkoutOverviewQueryVariables;
    skip?: boolean;
} | {
    skip: boolean;
});
export declare const GetUserWorkoutOverviewComponent: (props: GetUserWorkoutOverviewComponentProps) => JSX.Element;
export declare type GetUserWorkoutOverviewProps<TChildProps = {}> = ApolloReactHoc.DataProps<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables> & TChildProps;
export declare function withGetUserWorkoutOverview<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<TProps, GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables, GetUserWorkoutOverviewProps<TChildProps>>): (WrappedComponent: React.ComponentType<TProps & ApolloReactHoc.DataProps<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables> & TChildProps>) => React.ComponentClass<TProps, any>;
/**
 * __useGetUserWorkoutOverviewQuery__
 *
 * To run a query within a React component, call `useGetUserWorkoutOverviewQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserWorkoutOverviewQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserWorkoutOverviewQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export declare function useGetUserWorkoutOverviewQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables>): ApolloReactCommon.QueryResult<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables>;
export declare function useGetUserWorkoutOverviewLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables>): [(options?: ApolloReactHooks.QueryLazyOptions<GetUserWorkoutOverviewQueryVariables>) => void, ApolloReactCommon.QueryResult<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables>];
export declare type GetUserWorkoutOverviewQueryHookResult = ReturnType<typeof useGetUserWorkoutOverviewQuery>;
export declare type GetUserWorkoutOverviewLazyQueryHookResult = ReturnType<typeof useGetUserWorkoutOverviewLazyQuery>;
export declare type GetUserWorkoutOverviewQueryResult = ApolloReactCommon.QueryResult<GetUserWorkoutOverviewQuery, GetUserWorkoutOverviewQueryVariables>;
export declare const GetAllUserWorkoutsDocument: import("graphql").DocumentNode;
export declare type GetAllUserWorkoutsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables>, 'query'> & ({
    variables: GetAllUserWorkoutsQueryVariables;
    skip?: boolean;
} | {
    skip: boolean;
});
export declare const GetAllUserWorkoutsComponent: (props: GetAllUserWorkoutsComponentProps) => JSX.Element;
export declare type GetAllUserWorkoutsProps<TChildProps = {}> = ApolloReactHoc.DataProps<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables> & TChildProps;
export declare function withGetAllUserWorkouts<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<TProps, GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables, GetAllUserWorkoutsProps<TChildProps>>): (WrappedComponent: React.ComponentType<TProps & ApolloReactHoc.DataProps<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables> & TChildProps>) => React.ComponentClass<TProps, any>;
/**
 * __useGetAllUserWorkoutsQuery__
 *
 * To run a query within a React component, call `useGetAllUserWorkoutsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAllUserWorkoutsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAllUserWorkoutsQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export declare function useGetAllUserWorkoutsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables>): ApolloReactCommon.QueryResult<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables>;
export declare function useGetAllUserWorkoutsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables>): [(options?: ApolloReactHooks.QueryLazyOptions<GetAllUserWorkoutsQueryVariables>) => void, ApolloReactCommon.QueryResult<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables>];
export declare type GetAllUserWorkoutsQueryHookResult = ReturnType<typeof useGetAllUserWorkoutsQuery>;
export declare type GetAllUserWorkoutsLazyQueryHookResult = ReturnType<typeof useGetAllUserWorkoutsLazyQuery>;
export declare type GetAllUserWorkoutsQueryResult = ApolloReactCommon.QueryResult<GetAllUserWorkoutsQuery, GetAllUserWorkoutsQueryVariables>;
