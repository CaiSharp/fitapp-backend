import passport from 'passport';
import jwtStrat, { StrategyOptions } from 'passport-jwt';

import User from '@models/user';
const { JWT_SECRET } = process.env;

const jwtOptions: StrategyOptions = {
	jwtFromRequest: jwtStrat.ExtractJwt.fromAuthHeaderAsBearerToken(),
	secretOrKey: JWT_SECRET,
};
interface Token {
	user: {
		id: string;
		name: string;
		email: string;
	};
}

passport.use(
	new jwtStrat.Strategy(jwtOptions, async (token: Token, done) => {
		try {
			const user = await User.findById(token.user.id);

			if (user) {
				return done(false, user);
			} else {
				return done(null, false);
			}
		} catch (error) {
			return done(error, false);
		}
	}),
);

export default passport;
