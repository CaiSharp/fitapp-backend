import mongoose, { Model, Schema } from 'mongoose';

import { Workout } from '@fitapp/api/server';

import { exerciseWorkoutSchema } from './exersice';

export const workoutSchema = new Schema(
	{
		name: {
			type: String,
			required: true,
			minlength: 3,
		},
		date: {
			type: Date,
			required: true,
		},
		userId: { type: Schema.Types.ObjectId, ref: 'User', required: true },
		exercises: { type: [exerciseWorkoutSchema], required: true },
		duration: { type: Number, required: true },
	},
	{ timestamps: true },
);

export type WorkoutDocument = Workout &
	mongoose.Document & {
		_id: mongoose.Document['_id'];
	};

export type WorkoutModel = Model<WorkoutDocument>;

export default mongoose.model<WorkoutDocument, WorkoutModel>(
	'Workout',
	workoutSchema,
);
