import mongoose, { Model, Schema } from 'mongoose';

import { Exercise, SetType } from '@fitapp/api/server';

const setSchema = new Schema(
	{
		repetitions: {
			type: Number,
			required: true,
			min: [0, 'Your reps can`t be negative'],
		},
		weight: Number,
		type: {
			type: String,
			enum: [...Object.values(SetType)],
		},
	},
	{ id: false },
);

export const exerciseSchema = new Schema(
	{
		name: {
			type: String,
			required: true,
			minlength: 3,
		},
		sets: [setSchema],
		userId: {
			type: Schema.Types.ObjectId,
			ref: 'User',
			required: true,
		},
	},
	{ _id: true },
);

export const exerciseWorkoutSchema = new Schema(
	{
		_id: {
			type: Schema.Types.ObjectId,
			ref: 'Exercise',
			required: true,
		},
		name: {
			type: String,
			required: true,
			minlength: 3,
		},
		sets: [setSchema],
		userId: {
			type: Schema.Types.ObjectId,
			ref: 'User',
			required: true,
		},
	},
	{ _id: false },
);

export type ExerciseDocument = Exercise &
	mongoose.Document & {
		_id: mongoose.Document['_id'];
	};

export type ExerciseModel = Model<ExerciseDocument>;

export default mongoose.model<ExerciseDocument, ExerciseModel>(
	'Exercise',
	exerciseSchema,
);
