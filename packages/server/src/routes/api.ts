import express from 'express';
import passport from 'passport';

const router = express.Router();

router.post('/graphql', (req, res, next) => {
	passport.authenticate('jwt', { session: false }, (error, user) => {
		req.user = user;
		next();
	})(req, res);
});

export default router;
