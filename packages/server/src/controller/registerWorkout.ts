import { MutationRegisterWorkoutArgs, RequireFields } from '@fitapp/api/server';
import Exercise from '@root/src/models/exersice';
import { UserDocument } from '@models/user';
import Workout from '@models/workout';
import { ObjectId } from 'mongodb';

export const registerWorkout = async (
	user: UserDocument,
	args: RequireFields<
		MutationRegisterWorkoutArgs,
		'name' | 'date' | 'userId' | 'duration'
	>,
) => {
	//create workout with empty exercises first, update later
	const workout = await Workout.create({ ...args, exercises: [] });

	// updates existing or creates new exercises in exercise & user collections, sets exercises for workout document
	args.exercises.forEach(async exercise => {
		if (ObjectId.isValid(exercise.id)) {
			// needed to use the "new" options which returns the updated document
			const foundExercise = await Exercise.findByIdAndUpdate(
				exercise.id,
				exercise,
				{ new: true },
			).catch(() => {
				throw new Error(
					`Couldn't find or update the exercise in the exercise collection`,
				);
			});
			// update existing exercise if found & return
			if (foundExercise) {
				await workout
					.updateOne({ $push: { exercises: foundExercise } })
					.catch(() => {
						throw new Error(`Couldn't push the exercise to the workout.`);
					});
				return;
			}
		}
		// else if either not found or no valid ObjectId was given create the exercise
		const createdExercise = await Exercise.create({
			name: exercise.name,
			sets: exercise.sets,
			userId: user.id,
		});
		await user
			.updateOne({ $push: { exercises: createdExercise.id } })
			.catch(() => {
				throw new Error(`Couldn't update the user exercises`);
			});
		await workout.updateOne({ $push: { exercises: createdExercise } });
		return;
	});

	await user.updateOne({ $push: { workouts: workout.id } }).catch(() => {
		throw new Error(`Couldn't update the user workouts`);
	});

	return workout;
};
