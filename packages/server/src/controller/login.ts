import { AuthenticationError } from 'apollo-server-express';

import User from '@models/user';
import { createJWT } from '@services/sharedUtils';

export const login = async (
	username: string,
	password: string,
): Promise<string> => {
	const user = await User.findOne({
		username,
	});

	if (!user) {
		throw new AuthenticationError('Not a valid username');
	}

	const validPassword = await user.isValidPassword(password);
	if (!validPassword) {
		throw new AuthenticationError('Not a valid password');
	}

	return createJWT(user);
};
