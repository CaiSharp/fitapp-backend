import { makeExecutableSchema } from 'apollo-server-express';

import { resolvers } from './resolvers';
import { AuthDirective, RoleDirective } from './directives';

import { typeDefs } from '@fitapp/api/server';

export const schema = makeExecutableSchema({
	typeDefs,
	resolvers,
	schemaDirectives: {
		auth: AuthDirective,
		role: RoleDirective,
	},
});
