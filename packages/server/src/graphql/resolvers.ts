import { ObjectID } from 'mongodb';

import { login, registerWorkout, signup } from '@controller/index';
import { Resolvers, Role } from '@fitapp/api/server';
import User from '@models/user';
import Workout from '@models/workout';
import Exercise from '@root/src/models/exersice';
import { isObjectId, isUserId } from '@services/sharedUtils';

export const resolvers: Resolvers = {
	Query: {
		getAllUsers: async (parent, args) => {
			return User.find({});
		},
		getUserById: async (parent, args) => {
			const { id } = args;
			isObjectId(id);
			return User.findOne({ _id: id })
				.populate('workouts')
				.populate('exercises');
		},
		getWorkoutsByUserId: async (parent, args, context, info) => {
			const { userId } = args;
			isObjectId(userId);
			return Workout.find({ userId });
		},
		getWorkoutById: async (parent, args) => {
			const { id } = args;
			isObjectId(id);
			return Workout.findOne({ _id: id });
		},
		getWorkoutOverviewStats: async (parent, args) => {
			const { userId } = args;
			isObjectId(userId);
			const result: {
				workoutCount: number;
				duration: number;
			}[] = await Workout.aggregate([
				{ $match: { userId: new ObjectID(userId) } },
				{
					$group: {
						_id: { userId },
						workoutCount: { $sum: 1 },
						duration: { $sum: '$duration' },
					},
				},
			]);
			return result[0];
		},
		getExercisesByUserId: async (parent, args) => {
			const { userId } = args;
			isObjectId(userId);
			return Exercise.find({ userId });
		},
	},
	Mutation: {
		login: async (parent, args) => {
			const { username, password } = args;
			const token = await login(username, password);
			return { token };
		},
		registerWorkout: async (parent, args) => {
			//TODO: Transaction
			const { userId } = args;
			const user = await isUserId(userId);
			const workout = await registerWorkout(user, args);
			return workout;
		},
		signUp: async (parent, args) => {
			const token = await signup(args, [Role.User]);
			return { token };
		},
	},
};
