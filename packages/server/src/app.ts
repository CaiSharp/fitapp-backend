import { ApolloServer } from 'apollo-server-express';
import cors from 'cors';
import express from 'express';
import mongoose from 'mongoose';

import { schema } from '@graphql/createSchema';
import passport from '@middleware/passport';
import Exercise from '@root/src/models/exersice';
import Workout from '@root/src/models/workout';
import User from '@root/src/models/user';
import authRouter from '@routes/api';

const { NODE_ENV, APP_PORT, DB_NAME, DB_URL, FRONTEND_URL } = process.env;
const IN_PROD = NODE_ENV === 'production';
const apollo = new ApolloServer({
	schema,
	playground: !IN_PROD,
	context: ({ req }) => ({ user: req.user }),
});

const app = express();

app.disable('x-powered-by');
app.use(
	cors({
		origin: '*',
		credentials: false,
	}),
);
app.use(passport.initialize());
app.use(authRouter);
//disables included cors config in apollo, so custom one works
apollo.applyMiddleware({
	app,
	cors: false,
});

(async () => {
	try {
		await mongoose.connect(DB_URL, {
			dbName: DB_NAME,
			useNewUrlParser: true,
			useCreateIndex: true,
			useUnifiedTopology: true,
			useFindAndModify: false,
		});

		const db = mongoose.connection;

		//instantiate all Collections
		Exercise.createCollection();
		Workout.createCollection();
		User.createCollection();

		//TODO: if exercise collection empty, populate with default, should be used as basis for user field "exercises"
		//Exercise.create({ name: 'Test' });

		app.listen({ port: APP_PORT }, async () => {
			try {
				console.log(
					`🚀 Server ready at ${FRONTEND_URL}:${APP_PORT}${apollo.graphqlPath}`,
				);
			} catch (errorServer) {
				console.error('Server error', errorServer);
			}
		});
	} catch (errorDb) {
		console.error('Database error', errorDb);
	}
})();
