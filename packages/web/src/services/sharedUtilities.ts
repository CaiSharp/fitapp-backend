import { format } from 'date-fns';

// needed because date get's returned as number string, e.g. '12321313414' and all formats should be handled
export const formatDate = (date: string) =>
	isNaN(+date)
		? format(new Date(date), 'dd/MM/yyyy')
		: format(+date, 'dd/MM/yyyy');
