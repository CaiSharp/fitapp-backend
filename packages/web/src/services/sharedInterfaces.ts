import { Role } from '@fitapp/api/client';

export type User = {
	id: string;
	username: string;
	email: string;
	roles: Role[];
};
