import jwtDecode from 'jwt-decode';
import React from 'react';
import { User } from 'services/sharedInterfaces';

export const initUser = (): User | null => {
	const token = localStorage.getItem('token');
	if (token) {
		const { user }: { user: User } = jwtDecode(token);
		return user;
	} else {
		return null;
	}
};

export const logout = (
	stateSetter: React.Dispatch<React.SetStateAction<User | null>>,
) => {
	localStorage.removeItem('token');
	stateSetter(null);
};

export const AuthContext = React.createContext({
	user: { id: '', username: '', email: '', roles: [] } as User | null,
	updateUser: (user: User | null): void => {},
	logout: (): void => {},
});
