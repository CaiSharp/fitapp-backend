import { Layout, Menu } from 'antd';
import { AuthContext } from 'context/index';
import React, { useContext } from 'react';
import { Link, Redirect, Route, Switch } from 'react-router-dom';
import { Home, Login, Profile, SignUp, Workouts } from 'routes';

export const App: React.FC = props => {
	const auth = useContext(AuthContext);

	return (
		<Layout style={{ minHeight: '100vh' }}>
			<Layout.Header style={{ display: 'flex', justifyContent: 'flex-start' }}>
				<div className="logo" />
				<Menu
					theme="dark"
					mode="horizontal"
					style={{ lineHeight: '64px', marginRight: 'auto' }}
					selectable={false}
				>
					<Menu.Item>
						<Link to="/">FitApp</Link>
					</Menu.Item>
				</Menu>
				<Menu
					theme="dark"
					mode="horizontal"
					style={{ lineHeight: '64px' }}
					selectable={false}
				>
					{auth.user && (
						<Menu.Item>
							<Link to="/workouts">Workouts</Link>
						</Menu.Item>
					)}
					{auth.user && (
						<Menu.Item>
							<Link to="/profile">{auth.user.username}</Link>
						</Menu.Item>
					)}
					{auth.user && <Menu.Item onClick={auth.logout}>Logout</Menu.Item>}
					{!auth.user && (
						<Menu.Item>
							<Link to="/login">Login</Link>
						</Menu.Item>
					)}
					{!auth.user && (
						<Menu.Item>
							<Link to="/signup">Signup</Link>
						</Menu.Item>
					)}
				</Menu>
			</Layout.Header>
			<Layout.Content
				style={{
					display: 'flex',
					alignItems: 'center',
					justifyContent: 'center',
				}}
			>
				<Switch>
					{!auth.user && <Route exact path={'/login'} component={Login} />}
					{!auth.user && <Route exact path={'/signup'} component={SignUp} />}
					{auth.user && <Route exact path={'/profile'} component={Profile} />}
					{auth.user && <Route path={'/workouts'} component={Workouts} />}
					<Route
						path={'/'}
						render={() => (auth.user ? <Home /> : <Redirect to="/login" />)}
					/>
				</Switch>
			</Layout.Content>
		</Layout>
	);
};
