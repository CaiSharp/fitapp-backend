import React from 'react';
import { render } from '@testing-library/react';
import { MockedProvider } from '@apollo/react-testing';
import { Login } from './Login';
import { MemoryRouter } from 'react-router';

describe('<Login/>', () => {
	it('renders and matches snapshot', () => {
		const { baseElement } = render(
			<MemoryRouter initialEntries={['/login']}>
				<MockedProvider>
					<Login />
				</MockedProvider>
			</MemoryRouter>,
		);
		expect(baseElement).toMatchSnapshot();
	});
});
