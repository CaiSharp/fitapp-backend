import { Form, Input, Button } from 'antd';

import { AuthContext } from 'context/index';
import jwtDecode from 'jwt-decode';
import React, { useContext, useCallback } from 'react';
import { Link } from 'react-router-dom';
import { User } from 'services/sharedInterfaces';

import { useMutation } from '@apollo/react-hooks';
import { LoginDocument, LoginMutation } from '@fitapp/api/client';

type FormValues = {
	username: string;
	password: string;
};

export const LoginForm: React.FC = props => {
	const [login] = useMutation<LoginMutation>(LoginDocument, {
		errorPolicy: 'all',
	});
	const auth = useContext(AuthContext);

	const loginHandler = useCallback(
		async (values: FormValues) => {
			const res = await login({
				variables: {
					username: values.username,
					password: values.password,
				},
			});

			if (res.data?.login) {
				window.localStorage.setItem('token', res.data.login.token);
				const decodedToken: {
					user: User;
					exp: number;
					iat: number;
				} = jwtDecode(res.data.login.token);
				auth.updateUser(decodedToken.user);
			}
		},
		[auth, login],
	);

	return (
		<Form
			onFinish={values => loginHandler(values as FormValues)}
			layout="vertical"
		>
			<Form.Item
				label="Username"
				name="username"
				rules={[{ required: true, message: 'Please input your username!' }]}
			>
				<Input data-testid="LoginUsername" />
			</Form.Item>

			<Form.Item
				label="Password"
				name="password"
				rules={[{ required: true, message: 'Please input your password!' }]}
			>
				<Input.Password data-testid="LoginPassword" />
			</Form.Item>

			<div style={{ display: 'flex', justifyContent: 'space-between' }}>
				<Form.Item>
					<Button
						type="primary"
						htmlType="submit"
						data-testid="LoginSubmitButton"
					>
						Submit
					</Button>
				</Form.Item>
				<Link to={'/signup'}>Sign Up</Link>
			</div>
		</Form>
	);
};
