import React from 'react';
import { render, fireEvent, act, wait } from '@testing-library/react';
import { MockedProvider, MockedResponse } from '@apollo/react-testing';
import { LoginForm } from './LoginForm';
import { MemoryRouter } from 'react-router';
import { LoginDocument, Role } from '@fitapp/api/client';
import { AuthContext } from 'context';

jest.mock('jwt-decode', () =>
	jest.fn(() => ({ user: 'userObject', exp: 111, iat: 111 })),
);

const mockLogin: MockedResponse[] = [
	{
		request: {
			query: LoginDocument,
			variables: {
				username: 'alina',
				password: 'password',
			},
		},
		result: {
			data: {
				login: {
					token: 'testToken',
				},
			},
		},
	},
];

describe('<LoginForm/>', () => {
	it('renders all form elements', () => {
		const { getByTestId } = render(
			<MemoryRouter>
				<MockedProvider>
					<LoginForm />
				</MockedProvider>
			</MemoryRouter>,
		);
		const usernameField = getByTestId('LoginUsername');
		const passwordField = getByTestId('LoginPassword');
		const submitButton = getByTestId('LoginSubmitButton');

		expect(usernameField).toBeTruthy();
		expect(passwordField).toBeTruthy();
		expect(submitButton).toBeTruthy();
	});
	it('submits the login if all forms are filled in & validated', async () => {
		const updateUser = jest.fn();
		const logout = jest.fn();
		Storage.prototype.setItem = jest.fn();

		const { getByTestId } = render(
			<AuthContext.Provider
				value={{
					user: {
						email: 'test@email.com',
						username: 'testUsername',
						id: 'testUserID',
						roles: ['USER' as Role],
					},
					updateUser,
					logout,
				}}
			>
				<MemoryRouter>
					<MockedProvider mocks={mockLogin} addTypename={false}>
						<LoginForm />
					</MockedProvider>
				</MemoryRouter>
			</AuthContext.Provider>,
		);

		const usernameField = getByTestId('LoginUsername');
		const passwordField = getByTestId('LoginPassword');
		const submitButton = getByTestId('LoginSubmitButton');

		act(() => {
			fireEvent.change(usernameField, { target: { value: 'alina' } });
			fireEvent.change(passwordField, { target: { value: 'password' } });
			fireEvent.click(submitButton);
		});

		await wait(() => {
			expect(window.localStorage.setItem).toHaveBeenLastCalledWith(
				'token',
				'testToken',
			);
			expect(updateUser).toHaveBeenLastCalledWith('userObject');
		});
	});
	it("doesn't submit if password is empty", async () => {
		const updateUser = jest.fn();
		const logout = jest.fn();
		Storage.prototype.setItem = jest.fn();

		const { getByTestId, getByText } = render(
			<AuthContext.Provider
				value={{
					user: {
						email: 'test@email.com',
						username: 'testUsername',
						id: 'testUserID',
						roles: ['USER' as Role],
					},
					updateUser,
					logout,
				}}
			>
				<MemoryRouter>
					<MockedProvider mocks={mockLogin} addTypename={false}>
						<LoginForm />
					</MockedProvider>
				</MemoryRouter>
			</AuthContext.Provider>,
		);

		const usernameField = getByTestId('LoginUsername');
		const submitButton = getByTestId('LoginSubmitButton');

		act(() => {
			fireEvent.change(usernameField, { target: { value: 'alina' } });
			fireEvent.click(submitButton);
		});

		await wait(() => {
			expect(window.localStorage.setItem).toHaveBeenCalledTimes(0);
			expect(updateUser).toHaveBeenCalledTimes(0);
			expect(getByText('Please input your password!')).toBeTruthy();
		});
	});
	it("doesn't submit if username is empty", async () => {
		const updateUser = jest.fn();
		const logout = jest.fn();
		Storage.prototype.setItem = jest.fn();

		const { getByTestId, getByText } = render(
			<AuthContext.Provider
				value={{
					user: {
						email: 'test@email.com',
						username: 'testUsername',
						id: 'testUserID',
						roles: ['USER' as Role],
					},
					updateUser,
					logout,
				}}
			>
				<MemoryRouter>
					<MockedProvider mocks={mockLogin} addTypename={false}>
						<LoginForm />
					</MockedProvider>
				</MemoryRouter>
			</AuthContext.Provider>,
		);

		const passwordField = getByTestId('LoginPassword');
		const submitButton = getByTestId('LoginSubmitButton');

		act(() => {
			fireEvent.change(passwordField, { target: { value: 'password' } });
			fireEvent.click(submitButton);
		});

		await wait(() => {
			expect(window.localStorage.setItem).toHaveBeenCalledTimes(0);
			expect(updateUser).toHaveBeenCalledTimes(0);
			expect(getByText('Please input your username!')).toBeTruthy();
		});
	});
});
