import { Col, Row, Typography } from 'antd';
import React from 'react';

import { LoginForm } from './LoginForm/LoginForm';

export const Login: React.FC = () => (
	<Row align="middle" justify="center" style={{ display: 'flex', flex: 1 }}>
		<Col span={6}>
			<Typography.Title level={2}>Login</Typography.Title>
			<LoginForm />
		</Col>
	</Row>
);
