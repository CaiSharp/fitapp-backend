import { Col, Row, Typography } from 'antd';
import React from 'react';

import { SignUpForm } from './SignUpForm/SignUpForm';

export const SignUp: React.FC = () => (
	<Row align="middle" justify="center" style={{ display: 'flex', flex: 1 }}>
		<Col span={6}>
			<Typography.Title level={2}>Sign Up</Typography.Title>
			<SignUpForm />
		</Col>
	</Row>
);
