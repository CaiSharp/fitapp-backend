import React from 'react';
import { render, fireEvent, act, wait } from '@testing-library/react';
import { MockedProvider, MockedResponse } from '@apollo/react-testing';
import { SignUpForm } from './SignUpForm';
import { MemoryRouter } from 'react-router';
import { SignUpDocument, Role } from '@fitapp/api/client';
import { AuthContext } from 'context';

jest.mock('jwt-decode', () =>
	jest.fn(() => ({ user: 'userObject', exp: 111, iat: 111 })),
);

const mockLogin: MockedResponse[] = [
	{
		request: {
			query: SignUpDocument,
			variables: {
				username: 'alina',
				password: 'password',
				email: 'test@email.com',
			},
		},
		result: {
			data: {
				signUp: {
					token: 'testToken',
				},
			},
		},
	},
];

describe('<SignUpForm/>', () => {
	it('renders all form elements', () => {
		const { getByTestId } = render(
			<MemoryRouter>
				<MockedProvider>
					<SignUpForm />
				</MockedProvider>
			</MemoryRouter>,
		);

		const emailField = getByTestId('SignUpEmail');
		const usernameField = getByTestId('SignUpUsername');
		const nameField = getByTestId('SignUpName');
		const passwordField = getByTestId('SignUpPassword');
		const confirmPasswordField = getByTestId('SignUpConmfirmPassword');
		const submitButton = getByTestId('SignUpSubmitButton');

		expect(emailField).toBeTruthy();
		expect(usernameField).toBeTruthy();
		expect(nameField).toBeTruthy();
		expect(passwordField).toBeTruthy();
		expect(confirmPasswordField).toBeTruthy();
		expect(submitButton).toBeTruthy();
	});
	it('submits the login if all required fields are filled in & validated', async () => {
		const updateUser = jest.fn();
		const logout = jest.fn();
		Storage.prototype.setItem = jest.fn();

		const { getByTestId } = render(
			<AuthContext.Provider
				value={{
					user: {
						email: 'test@email.com',
						username: 'testUsername',
						id: 'testUserID',
						roles: ['USER' as Role],
					},
					updateUser,
					logout,
				}}
			>
				<MemoryRouter>
					<MockedProvider mocks={mockLogin} addTypename={false}>
						<SignUpForm />
					</MockedProvider>
				</MemoryRouter>
			</AuthContext.Provider>,
		);

		const emailField = getByTestId('SignUpEmail');
		const usernameField = getByTestId('SignUpUsername');
		const passwordField = getByTestId('SignUpPassword');
		const confirmPasswordField = getByTestId('SignUpConmfirmPassword');
		const submitButton = getByTestId('SignUpSubmitButton');

		act(() => {
			fireEvent.change(emailField, { target: { value: 'test@email.com' } });
			fireEvent.change(usernameField, { target: { value: 'alina' } });
			fireEvent.change(usernameField, { target: { value: 'alina' } });
			fireEvent.change(passwordField, { target: { value: 'password' } });
			fireEvent.change(confirmPasswordField, { target: { value: 'password' } });
			fireEvent.click(submitButton);
		});

		await wait(() => {
			expect(window.localStorage.setItem).toHaveBeenLastCalledWith(
				'token',
				'testToken',
			);
			expect(updateUser).toHaveBeenLastCalledWith('userObject');
		});
	});

	it('does not submit if one of the required fields is missing', async () => {
		const updateUser = jest.fn();
		const logout = jest.fn();
		Storage.prototype.setItem = jest.fn();

		const { getByTestId, getByText } = render(
			<AuthContext.Provider
				value={{
					user: {
						email: 'test@email.com',
						username: 'testUsername',
						id: 'testUserID',
						roles: ['USER' as Role],
					},
					updateUser,
					logout,
				}}
			>
				<MemoryRouter>
					<MockedProvider mocks={mockLogin} addTypename={false}>
						<SignUpForm />
					</MockedProvider>
				</MemoryRouter>
			</AuthContext.Provider>,
		);

		const usernameField = getByTestId('SignUpUsername');
		const passwordField = getByTestId('SignUpPassword');
		const confirmPasswordField = getByTestId('SignUpConmfirmPassword');
		const submitButton = getByTestId('SignUpSubmitButton');

		act(() => {
			fireEvent.change(usernameField, { target: { value: 'alina' } });
			fireEvent.change(usernameField, { target: { value: 'alina' } });
			fireEvent.change(passwordField, { target: { value: 'password' } });
			fireEvent.change(confirmPasswordField, { target: { value: 'password' } });
			fireEvent.click(submitButton);
		});

		await wait(() => {
			expect(window.localStorage.setItem).toHaveBeenCalledTimes(0);
			expect(updateUser).toHaveBeenCalledTimes(0);
			expect(getByText('Please input your E-mail!')).toBeTruthy();
		});
	});

	it('shows an error if the passwords dont match', async () => {
		const updateUser = jest.fn();
		const logout = jest.fn();
		Storage.prototype.setItem = jest.fn();

		const { getByTestId, getByText } = render(
			<AuthContext.Provider
				value={{
					user: {
						email: 'test@email.com',
						username: 'testUsername',
						id: 'testUserID',
						roles: ['USER' as Role],
					},
					updateUser,
					logout,
				}}
			>
				<MemoryRouter>
					<MockedProvider mocks={mockLogin} addTypename={false}>
						<SignUpForm />
					</MockedProvider>
				</MemoryRouter>
			</AuthContext.Provider>,
		);

		const passwordField = getByTestId('SignUpPassword');
		const confirmPasswordField = getByTestId('SignUpConmfirmPassword');

		act(() => {
			fireEvent.change(passwordField, { target: { value: 'password' } });
			fireEvent.change(confirmPasswordField, {
				target: { value: 'password1' },
			});
		});

		await wait(() => {
			expect(
				getByText('The two passwords that you entered do not match!'),
			).toBeTruthy();
		});
	});
});
