import { Form, Input, Tooltip, Button } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';

import { AuthContext } from 'context/index';
import jwtDecode from 'jwt-decode';
import React, { useContext, useCallback } from 'react';
import { Link } from 'react-router-dom';
import { User } from 'services/sharedInterfaces';

import { useMutation } from '@apollo/react-hooks';
import { SignUpDocument, SignUpMutation } from '@fitapp/api/client';

type FormValues = {
	email: string;
	username: string;
	name?: string;
	password: string;
};

export const SignUpForm: React.FC = props => {
	const [signUp] = useMutation<SignUpMutation>(SignUpDocument, {
		errorPolicy: 'all',
	});
	const auth = useContext(AuthContext);
	const [form] = Form.useForm();

	const signUpHandler = useCallback(
		async (values: FormValues) => {
			const res = await signUp({
				variables: {
					username: values.username,
					password: values.password,
					email: values.email,
					name: values.name,
				},
			});
			if (res.data?.signUp) {
				window.localStorage.setItem('token', res.data.signUp.token);
				const decodedToken: {
					user: User;
					exp: number;
					iat: number;
				} = jwtDecode(res.data.signUp.token);
				auth.updateUser(decodedToken.user);
			}
		},
		[auth, signUp],
	);

	return (
		<Form
			form={form}
			name="register"
			onFinish={values => signUpHandler(values as FormValues)}
			scrollToFirstError
			layout="vertical"
		>
			<Form.Item
				name="email"
				label="E-mail"
				rules={[
					{
						type: 'email',
						message: 'The input is not valid E-mail!',
					},
					{
						required: true,
						message: 'Please input your E-mail!',
					},
				]}
			>
				<Input data-testid="SignUpEmail" />
			</Form.Item>

			<Form.Item
				name="username"
				label={
					<span>
						Username&nbsp;
						<Tooltip title="What do you want your username to be?">
							<QuestionCircleOutlined />
						</Tooltip>
					</span>
				}
				rules={[
					{
						required: true,
						message: 'Please input your username!',
						whitespace: true,
					},
				]}
			>
				<Input data-testid="SignUpUsername" />
			</Form.Item>

			<Form.Item
				data-testid="SignUpName"
				name="name"
				label={
					<span>
						Name&nbsp;
						<Tooltip title="What's your name?">
							<QuestionCircleOutlined />
						</Tooltip>
					</span>
				}
				rules={[
					{
						message: 'Please input your name!',
						whitespace: true,
					},
				]}
			>
				<Input />
			</Form.Item>

			<Form.Item
				name="password"
				label="Password"
				rules={[
					{
						required: true,
						message: 'Please input your password!',
					},
				]}
				hasFeedback
			>
				<Input.Password data-testid="SignUpPassword" />
			</Form.Item>

			<Form.Item
				name="confirm"
				label="Confirm Password"
				dependencies={['password']}
				hasFeedback
				rules={[
					{
						required: true,
						message: 'Please confirm your password!',
					},
					({ getFieldValue }) => ({
						validator(rule, value) {
							if (!value || getFieldValue('password') === value) {
								return Promise.resolve();
							}
							return Promise.reject(
								'The two passwords that you entered do not match!',
							);
						},
					}),
				]}
			>
				<Input.Password data-testid="SignUpConmfirmPassword" />
			</Form.Item>

			<div style={{ display: 'flex', justifyContent: 'space-between' }}>
				<Form.Item>
					<Button
						data-testid="SignUpSubmitButton"
						type="primary"
						htmlType="submit"
					>
						Register
					</Button>
				</Form.Item>
				<Link to={'/login'}>Login</Link>
			</div>
		</Form>
	);
};
