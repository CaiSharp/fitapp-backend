import React from 'react';
import { render } from '@testing-library/react';
import { MockedProvider } from '@apollo/react-testing';
import { SignUp } from './SignUp';
import { MemoryRouter } from 'react-router';

describe('<SignUp/>', () => {
	it('renders and matches snapshot', () => {
		const { baseElement } = render(
			<MemoryRouter initialEntries={['/SignUp']}>
				<MockedProvider>
					<SignUp />
				</MockedProvider>
			</MemoryRouter>,
		);
		expect(baseElement).toMatchSnapshot();
	});
});
