export { Login } from './Login/Login';
export { SignUp } from './SignUp/SignUp';
export { Home } from './Home/Home';
export { Profile } from './Profile/Profile';
export { Workouts } from './Workouts/Workouts';
