import React from 'react';
import { render, fireEvent, act } from '@testing-library/react';
import { MockedProvider, MockedResponse } from '@apollo/react-testing';
import { WorkoutDetail } from './WorkoutDetail';
import Router, { MemoryRouter } from 'react-router';
import { GetWorkoutDocument } from '@fitapp/api/client';

const mockLocation = {
	id: '123',
};

beforeEach(() => {
	jest.spyOn(Router, 'useParams').mockReturnValue(mockLocation);
});

const mocksWithExercise: MockedResponse[] = [
	{
		request: {
			query: GetWorkoutDocument,
			variables: {
				id: '123',
			},
		},
		result: {
			data: {
				getWorkoutById: {
					id: '1',
					duration: 5,
					exercises: [
						{
							id: '123',
							name: 'ExerciseName',
							sets: [
								{
									repetitions: 12,
									weight: 70,
									type: 'NORMAL',
								},
							],
						},
					],
					date: new Date(2020, 10, 10),
					name: 'Alinas-Workout',
				},
			},
		},
	},
];

const mocksWithoutExercise: MockedResponse[] = [
	{
		request: {
			query: GetWorkoutDocument,
			variables: {
				id: '123',
			},
		},
		result: {
			data: {
				getWorkoutById: {
					id: '1',
					duration: 5,
					exercises: [],
					date: new Date(2020, 10, 10),
					name: 'Alinas-Workout',
				},
			},
		},
	},
];

describe('<WorkoutDetail/>', () => {
	it('renders all elements', async () => {
		const { findByText, debug } = render(
			<MemoryRouter>
				<MockedProvider mocks={mocksWithExercise} addTypename={false}>
					<WorkoutDetail />
				</MockedProvider>
			</MemoryRouter>,
		);

		const workoutName = await findByText('Alinas-Workout');
		const workoutDuration = await findByText('5');
		const workoutDate = await findByText('10/11/2020');
		const exerciseName = await findByText('ExerciseName');

		expect(workoutName).toBeTruthy();
		expect(workoutDuration).toBeTruthy();
		expect(workoutDate).toBeTruthy();
		expect(exerciseName).toBeTruthy();
	});
	it('expands and shows exercise sets', async () => {
		const { findByLabelText, findByText } = render(
			<MemoryRouter>
				<MockedProvider mocks={mocksWithExercise} addTypename={false}>
					<WorkoutDetail />
				</MockedProvider>
			</MemoryRouter>,
		);

		const expandRow = await findByLabelText('Expand row');

		act(() => {
			fireEvent.click(expandRow);
		});

		const repetitionsValue = await findByText('12');
		const weightValue = await findByText('70');
		const typeValue = await findByText('NORMAL');

		expect(repetitionsValue).toBeTruthy();
		expect(weightValue).toBeTruthy();
		expect(typeValue).toBeTruthy();
	});
	it('shows no data if no exercises', async () => {
		const { findByText } = render(
			<MemoryRouter>
				<MockedProvider mocks={mocksWithoutExercise} addTypename={false}>
					<WorkoutDetail />
				</MockedProvider>
			</MemoryRouter>,
		);

		const workoutNoExercises = await findByText('No Data');

		expect(workoutNoExercises).toBeTruthy();
	});
});
