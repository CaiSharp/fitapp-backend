import { Descriptions, Table, Typography } from 'antd';

import React from 'react';
import { useParams } from 'react-router-dom';
import { formatDate } from 'services/sharedUtilities';

import { useQuery } from '@apollo/react-hooks';
import { GetWorkoutDocument, GetWorkoutQuery } from '@fitapp/api/client';

export const WorkoutDetail: React.FC = () => {
	const { id } = useParams();
	const { data } = useQuery<GetWorkoutQuery>(GetWorkoutDocument, {
		variables: { id: id ?? '' },
	});
	if (!data?.getWorkoutById) return null;

	const { duration, exercises, date, name } = data.getWorkoutById;

	let columns, tableData;

	if (exercises) {
		columns = [
			{
				title: 'Exercises',
				dataIndex: 'title',
				key: 'exercises',
			},
		];
		tableData = exercises.map((el, index) => ({
			key: index,
			title: el.name,
			sets: el.sets,
		}));
	}

	return (
		<React.Fragment>
			<Typography.Title level={2}>Workout Details</Typography.Title>
			<Descriptions bordered column={1}>
				<Descriptions.Item data-testid="WorkoutName" label="Name">
					{name}
				</Descriptions.Item>
				{
					<Descriptions.Item data-testid="WorkoutDuration" label="Duration">
						{duration}
					</Descriptions.Item>
				}
				<Descriptions.Item data-testid="WorkoutDate" label="Date">
					{formatDate(date)}
				</Descriptions.Item>
			</Descriptions>
			{exercises && (
				<Table
					columns={columns}
					dataSource={tableData}
					style={{ marginTop: 20 }}
					expandedRowRender={(exercise) => {
						const columns = [
							{
								title: 'Set Number',
								dataIndex: 'key',
								key: 'key',
							},
							{
								title: 'Repetitions',
								dataIndex: 'repetitions',
								key: 'repetitions',
							},
							{
								title: 'Weight',
								dataIndex: 'weight',
								key: 'weight',
							},
							{
								title: 'Type',
								dataIndex: 'type',
								key: 'type',
							},
						];
						const data = exercise.sets?.map((el, index) => ({
							key: index,
							repetitions: el.repetitions,
							weight: el.weight,
							type: el.type,
						}));
						return (
							<Table
								data-testid="WorkoutExercises"
								columns={columns}
								dataSource={data}
							/>
						);
					}}
				/>
			)}
		</React.Fragment>
	);
};
