import { Col, Row } from 'antd';
import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { WorkoutDetail } from './WorkoutDetail/WorkoutDetail';
import { WorkoutForm } from './WorkoutForm/WorkoutForm';
import { WorkoutsOverview } from './WorkoutsOverview/WorkoutsOverview';

export const Workouts: React.FC = () => (
	<Row align="middle" justify="center" style={{ display: 'flex', flex: 1 }}>
		<Col span={12}>
			<Switch>
				<Route path={'/workouts/new'} exact component={WorkoutForm} />
				<Route path={'/workouts'} exact component={WorkoutsOverview} />
				<Route path={'/workouts/:id'} exact component={WorkoutDetail} />
			</Switch>
		</Col>
	</Row>
);
