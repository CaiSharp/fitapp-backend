import { Col, Row, Statistic } from 'antd';
import { AuthContext } from 'context';
import React, { useContext } from 'react';

import { useQuery } from '@apollo/react-hooks';
import { GetUserWorkoutOverviewDocument } from '@fitapp/api/client';

export const StatSummary: React.FC = () => {
	const auth = useContext(AuthContext);
	const { data } = useQuery(GetUserWorkoutOverviewDocument, {
		variables: { userId: auth.user ? auth.user.id : '' },
	});
	if (!data?.getWorkoutOverviewStats) return null;

	const { duration, workoutCount } = data.getWorkoutOverviewStats;

	return (
		<React.Fragment>
			<Row>
				<Col span={6}>
					<Statistic title="Total Minutes Trained" value={duration} />
				</Col>
				<Col span={6}>
					<Statistic title="Total Workout Count" value={workoutCount} />
				</Col>
			</Row>
		</React.Fragment>
	);
};
