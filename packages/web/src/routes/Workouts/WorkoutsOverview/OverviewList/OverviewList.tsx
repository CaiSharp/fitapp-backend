import { Table, Tag } from 'antd';
import { AuthContext } from 'context';
import { format } from 'date-fns';
import React, { useContext, useEffect } from 'react';
import { useHistory, useRouteMatch, useLocation } from 'react-router-dom';

import { useQuery } from '@apollo/react-hooks';
import {
	GetAllUserWorkoutsDocument,
	GetAllUserWorkoutsQuery,
} from '@fitapp/api/client';

export const OverviewList: React.FC = () => {
	const auth = useContext(AuthContext);
	const { path } = useRouteMatch();
	const location = useLocation();
	const history = useHistory();
	const { data, refetch } = useQuery<GetAllUserWorkoutsQuery>(
		GetAllUserWorkoutsDocument,
		{
			variables: { userId: auth.user ? auth.user.id : '' },
		},
	);

	useEffect(() => {
		refetch();
	}, [location, refetch]);

	if (!data?.getWorkoutsByUserId) return null;
	const workouts = data.getWorkoutsByUserId;

	const dataSource = [...workouts];

	type workoutType = NonNullable<
		NonNullable<GetAllUserWorkoutsQuery['getWorkoutsByUserId']>
	>[0]['__typename'];

	const columns = [
		{
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Duration',
			dataIndex: 'duration',
			key: 'duration',
		},
		{
			title: 'Type',
			dataIndex: '__typename',
			key: '__typename',
			render: (workoutType: workoutType) => (
				<Tag color={'green'} key={workoutType}>
					{workoutType}
				</Tag>
			),
		},
		{
			title: 'Date',
			dataIndex: 'createdAt',
			key: 'createdAt',
			render: (date: string) => format(parseInt(date), 'dd/MM/yyyy'),
		},
	];

	return (
		<Table
			dataSource={dataSource}
			columns={columns}
			rowKey={workout => workout.id}
			onRow={workout => ({
				onClick: event => history.push(`${path}/${workout.id}`),
			})}
		/>
	);
};
