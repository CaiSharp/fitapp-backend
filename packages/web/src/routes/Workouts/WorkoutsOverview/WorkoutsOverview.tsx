import { Row, Typography } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';

import { OverviewList } from './OverviewList/OverviewList';
import { StatSummary } from './StatSummary/StatSummary';

export const WorkoutsOverview: React.FC = () => (
	<React.Fragment>
		<Row>
			<Typography.Title level={2}>Workouts</Typography.Title>
		</Row>

		<StatSummary />
		<OverviewList />

		<Link to="workouts/new">New</Link>
	</React.Fragment>
);
