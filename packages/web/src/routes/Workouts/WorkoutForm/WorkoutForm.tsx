import { Button, notification, Form } from 'antd';
import { AuthContext } from 'context';
import moment from 'moment';
import React, { useContext, useState, useCallback } from 'react';
import { Link, useHistory } from 'react-router-dom';
import uuid from 'uuid/v1';
import { WorkoutFields } from './WorkoutFields/WorkoutFields';

import { useMutation, useQuery } from '@apollo/react-hooks';
import {
	Sets,
	Exercise,
	RegisterWorkoutDocument,
	GetUserExercisesDocument,
	GetUserExercisesQuery,
} from '@fitapp/api/client';

import { SelectExercise } from './SelectExercise/SelectExercise';
import { LabeledValue } from 'antd/lib/select';
import { ExerciseFields } from './ExerciseFields/ExerciseFields';

type FormExercisesValue = Omit<Exercise, 'sets' | 'id'> & {
	sets: { [id: string]: Omit<Sets, 'id'> };
};
type FormValues = {
	name: string;
	duration: number;
	exercises?: { [id: string]: FormExercisesValue };
	date: moment.Moment;
};

type ExerciseInput = GetUserExercisesQuery['getExercisesByUserId'][0] & {};

export const WorkoutForm: React.FC = props => {
	const [exerciseInputs, setExerciseInputs] = useState<ExerciseInput[]>([]);
	const auth = useContext(AuthContext);
	const history = useHistory();
	const [registerWorkouts] = useMutation(RegisterWorkoutDocument, {
		errorPolicy: 'all',
	});
	const [form] = Form.useForm();
	const { data } = useQuery<GetUserExercisesQuery>(GetUserExercisesDocument, {
		variables: { userId: auth?.user?.id ?? '' },
	});

	const addExercise = useCallback(
		(preDefinedExc?: LabeledValue | string | number) => {
			if (
				preDefinedExc &&
				typeof preDefinedExc !== 'string' &&
				typeof preDefinedExc !== 'number'
			) {
				const { key, label } = preDefinedExc;
				// get complete exercise data from fetched data instead of render
				const fetchedExercise = data?.getExercisesByUserId.filter(
					el => el.id === key,
				)[0];
				setExerciseInputs(prev => {
					const newArray = [...prev];
					newArray.push({
						id: key ?? '',
						name: `${label}`,
						sets: fetchedExercise?.sets ?? null,
					});
					return newArray;
				});
			} else {
				setExerciseInputs(prev => {
					const newArray = [...prev];
					newArray.push({ id: uuid(), name: '', sets: null });
					return newArray;
				});
			}
		},
		[data],
	);

	const removeExercise = useCallback((exerciseDeleteId: string) => {
		setExerciseInputs(prev => {
			const newArray = [...prev];
			return newArray.filter(exercise => exercise.id !== exerciseDeleteId);
		});
	}, []);

	const registerWorkoutHandler = useCallback(
		async (values: FormValues) => {
			if (!auth.user) return;

			const exercisesToSubmit: Exercise[] = [];
			if (values.exercises) {
				// transform objects holding exercises & sets to arrays for backend
				for (const [exerciseId, exerciseValue] of Object.entries(
					values.exercises,
				)) {
					const setsToSubmit: Sets[] = [];
					for (const setValues of Object.values(exerciseValue.sets)) {
						setsToSubmit.push(setValues);
					}
					exercisesToSubmit.push({
						...exerciseValue,
						id: exerciseId,
						userId: auth.user.id,
						sets: setsToSubmit,
					});
				}
			}

			//TODO: try catch | errorhandling
			await registerWorkouts({
				variables: {
					name: values.name,
					date: values.date.toISOString(),
					userId: auth.user.id,
					duration: values.duration,
					exercises: exercisesToSubmit,
				},
			});
			notification.open({
				message: 'Workoutregister',
				description: 'Your workout was created',
				duration: 2,
				onClick: () => history.goBack(),
			});
		},
		[registerWorkouts, auth.user, history],
	);

	return (
		<Form
			form={form}
			onFinish={values => registerWorkoutHandler(values as FormValues)}
			scrollToFirstError
			layout="vertical"
		>
			<WorkoutFields />
			<ExerciseFields
				exercises={exerciseInputs}
				onClickRemove={removeExercise}
				form={form}
			/>

			<SelectExercise
				onSelect={addExercise}
				onDeselect={removeExercise}
				exerciseOptions={data?.getExercisesByUserId}
				exerciseInputs={exerciseInputs}
			/>
			<Button
				onClick={() => addExercise()}
				block
				type="primary"
				style={{ marginTop: 20 }}
			>
				or create a new exercise
			</Button>

			<Form.Item
				style={{
					marginTop: 20,
				}}
			>
				<div
					style={{
						display: 'flex',
						flex: 1,
						justifyContent: 'space-between',
					}}
				>
					<Button type="primary" htmlType="submit">
						Submit Workout
					</Button>
					<Link to="/workouts">Workouts</Link>
				</div>
			</Form.Item>
		</Form>
	);
};
