import { MinusCircleOutlined } from '@ant-design/icons';

import { Button, InputNumber, Select, Form } from 'antd';

import React, { useState, useEffect, useCallback } from 'react';
import uuid from 'uuid/v1';

import { SetType, GetUserExercisesQuery } from '@fitapp/api/client';
import { FormInstance } from 'antd/lib/form';

type Set = NonNullable<
	NonNullable<
		NonNullable<NonNullable<GetUserExercisesQuery>['getExercisesByUserId']>[0]
	>['sets']
>[0];

type SetInput = Set & { id: string };
export type SetFieldsProps = {
	exercise: GetUserExercisesQuery['getExercisesByUserId'][0];
	form: FormInstance;
};
export const SetFields: React.FC<SetFieldsProps> = ({ exercise, form }) => {
	const [sets, setSets] = useState<SetInput[]>([]);

	const addSets = useCallback((preDefinedSet?: Set) => {
		if (preDefinedSet) {
			setSets(prev => {
				const newArray = [...prev];
				newArray.push({
					id: uuid(),
					weight: preDefinedSet.weight,
					repetitions: preDefinedSet.repetitions,
					type: preDefinedSet.type,
				});
				return newArray;
			});
		} else {
			setSets(prev => {
				const newArray = [...prev];
				newArray.push({
					id: uuid(),
					weight: 0,
					repetitions: 0,
					type: SetType.Normal,
				});
				return newArray;
			});
		}
	}, []);

	useEffect(() => {
		if (exercise?.sets && exercise.sets.length > 0) {
			exercise.sets?.forEach(set => addSets(set));
		} else {
			addSets();
		}
	}, [exercise, addSets]);

	const removeSets = useCallback((setKey: string) => {
		setSets(prev => {
			const newArray = [...prev];
			return newArray.filter(set => set.id !== setKey);
		});
	}, []);

	const setsToRender = sets.map(set => {
		// if the field isn't already set, initiate it | hydrate with data from server
		if (
			!form.getFieldValue(['exercises', `${exercise.id}`, 'sets', `${set.id}`])
		) {
			form.setFieldsValue({
				exercises: {
					[exercise.id]: {
						sets: {
							[set.id]: {
								weight: set.weight,
								repetitions: set.repetitions,
								type: set.type,
							},
						},
					},
				},
			});
		}

		return (
			<div
				key={set.id}
				style={{
					display: 'flex',
					flex: 1,
					justifyContent: 'space-between',
					alignItems: 'flex-end',
				}}
			>
				<Form.Item
					label="Repetitions"
					name={[
						'exercises',
						`${exercise.id}`,
						'sets',
						`${set.id}`,
						`repetitions`,
					]}
				>
					<InputNumber min={0} />
				</Form.Item>
				<Form.Item
					label="Weight"
					name={['exercises', `${exercise.id}`, 'sets', `${set.id}`, `weight`]}
				>
					<InputNumber />
				</Form.Item>
				<Form.Item
					label="Type"
					name={['exercises', `${exercise.id}`, 'sets', `${set.id}`, `type`]}
				>
					<Select>
						<Select.Option value={SetType.Normal}>Normal</Select.Option>
						<Select.Option value={SetType.Warmup}>Warmup</Select.Option>
						<Select.Option value={SetType.Failure}>Failure</Select.Option>
					</Select>
				</Form.Item>
				<Form.Item>
					<Button
						icon={<MinusCircleOutlined />}
						onClick={() => removeSets(set.id)}
						type="ghost"
					>
						Remove Set
					</Button>
				</Form.Item>
			</div>
		);
	});

	return (
		<React.Fragment>
			{setsToRender}
			<Form.Item>
				<Button
					onClick={() => addSets()}
					style={{ display: 'block' }}
					type="primary"
				>
					Add Set
				</Button>
			</Form.Item>
		</React.Fragment>
	);
};
