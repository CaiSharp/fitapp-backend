import React from 'react';
import { MinusCircleOutlined } from '@ant-design/icons';

import { Input, Form, Button } from 'antd';

import { GetUserExercisesQuery } from '@fitapp/api/client';
import { SetFields } from './SetFields/SetFields';
import { FormInstance } from 'antd/lib/form';
export type ExerciseFieldsProps = {
	exercises: GetUserExercisesQuery['getExercisesByUserId'];
	onClickRemove: (id: string) => void;
	form: FormInstance;
};

export const ExerciseFields: React.FC<ExerciseFieldsProps> = React.memo(
	({ exercises, onClickRemove, form }) => {
		const render = exercises.map(exercise => {
			// if the field isn't already set, initiate it | hydrate with data from server
			if (!form.getFieldValue(['exercises', `${exercise.id}`, `name`])) {
				form.setFieldsValue({
					exercises: { [exercise.id]: { name: exercise.name } },
				});
			}
			return (
				<React.Fragment key={exercise.id}>
					<Form.Item
						key={exercise.id}
						label="Exercise"
						name={['exercises', `${exercise.id}`, `name`]}
						rules={[
							{
								required: true,
								whitespace: true,
								message: 'Please input your exercise name',
							},
						]}
					>
						<Input />
					</Form.Item>
					<SetFields exercise={exercise} form={form} />
					<Form.Item>
						<Button
							icon={<MinusCircleOutlined />}
							onClick={() => onClickRemove(exercise.id)}
							type="ghost"
						>
							Remove Exercise
						</Button>
					</Form.Item>
				</React.Fragment>
			);
		});
		return <>{render}</>;
	},
);
