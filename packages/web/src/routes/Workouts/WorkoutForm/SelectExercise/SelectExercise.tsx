import React, { useState, useEffect, useCallback, useMemo } from 'react';
import { GetUserExercisesQuery } from '@fitapp/api/client';
import { Select, Button } from 'antd';
import { LabeledValue } from 'antd/lib/select';
import { differenceWith, intersectionWith } from 'lodash';
import { CloseCircleOutlined } from '@ant-design/icons';

type SelectExerciseProps = {
	exerciseOptions: GetUserExercisesQuery['getExercisesByUserId'] | undefined;
	exerciseInputs: GetUserExercisesQuery['getExercisesByUserId'][0][];
	onSelect: (value: string | number | LabeledValue) => void;
	onDeselect: (value: string) => void;
};

export const SelectExercise: React.FC<SelectExerciseProps> = React.memo(
	({ exerciseOptions, exerciseInputs, onSelect, onDeselect }) => {
		// handle selection state seperately from <Select/> component to control exercise render (add/remove)
		const [selection, setSelection] = useState<LabeledValue[]>();

		//remove selection entries if no corresponding exerciseInput is being rendered
		useEffect(() => {
			if (!selection) return;
			//create intersection of values from rendered exercises & current ui selection
			const intersectionValues = intersectionWith(
				selection,
				exerciseInputs,
				(selectionValue, exerciseValue) =>
					selectionValue.value === exerciseValue.id,
			);
			//modify selection state if intersection differs
			if (intersectionValues.length !== selection.length) {
				setSelection(intersectionValues);
			}
		}, [exerciseInputs, selection]);

		const onClear = useCallback(() => {
			if (!selection) return;
			selection.forEach(selectionValue =>
				onDeselect(`${selectionValue.value}`),
			);
			setSelection(undefined);
		}, [onDeselect, selection]);

		//filters out already rendered exercises
		const filteredExerciseOptions = useMemo(
			() =>
				differenceWith(
					exerciseOptions,
					exerciseInputs,
					(exerciseFromOption, exerciseFromInput) =>
						exerciseFromOption.id === exerciseFromInput.id,
				),
			[exerciseInputs, exerciseOptions],
		);

		if (!exerciseOptions || exerciseOptions?.length < 1) return null;
		return (
			<div style={{ display: 'flex', justifyContent: 'center' }}>
				<Select
					labelInValue
					showSearch
					placeholder="Select an existing exercise"
					autoClearSearchValue
					mode="multiple"
					maxTagTextLength={10}
					onSelect={value => onSelect(value)}
					onDeselect={value => onDeselect(`${value.value}`)}
					value={selection}
					filterOption
					onChange={value => setSelection(value)}
					style={{ flex: 1 }}
				>
					{filteredExerciseOptions.map(el => (
						<Select.Option value={el.id} key={el.id}>
							{el.name}
						</Select.Option>
					))}
				</Select>
				<Button
					//icon height is set to fix alignment bug
					icon={<CloseCircleOutlined style={{ height: 14 }} />}
					onClick={onClear}
				/>
			</div>
		);
	},
);
