import React from 'react';

import { Input, DatePicker, InputNumber, Form } from 'antd';

export const WorkoutFields: React.FC = props => (
	<>
		<Form.Item
			label="Name"
			name="name"
			rules={[{ required: true, message: 'Please input the workout name' }]}
		>
			<Input placeholder="Workout" />
		</Form.Item>
		<Form.Item
			label="Date"
			name="date"
			rules={[{ required: true, message: 'Please input the workout date' }]}
		>
			<DatePicker />
		</Form.Item>
		<Form.Item
			label="Duration"
			name="duration"
			rules={[{ required: true, message: 'Please input the workout duration' }]}
		>
			<InputNumber placeholder="min" min={0} />
		</Form.Item>
	</>
);
