import { Col, Row, Typography } from 'antd';
import React from 'react';

export const Home: React.FC = () => (
	<Row align="middle" justify="center" style={{ display: 'flex', flex: 1 }}>
		<Col span={6}>
			<Typography.Title level={2}>Home</Typography.Title>
		</Col>
	</Row>
);
