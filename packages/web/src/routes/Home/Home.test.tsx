import React from 'react';
import { render } from '@testing-library/react';
import { Home } from './Home';

describe('<Home/>', () => {
	it('renders and matches snapshot', () => {
		const { baseElement } = render(<Home />);
		expect(baseElement).toMatchSnapshot();
	});
});
