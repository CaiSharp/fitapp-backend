import { Col, Descriptions, Row, Typography } from 'antd';
import { AuthContext } from 'context';
import React, { useContext } from 'react';

import { useQuery } from '@apollo/react-hooks';
import { ProfileDocument, ProfileQuery } from '@fitapp/api/client';

import { formatDate } from 'services/sharedUtilities';

export const Profile: React.FC = () => {
	const auth = useContext(AuthContext);
	const { data } = useQuery<ProfileQuery>(ProfileDocument, {
		variables: { id: auth.user ? auth.user.id : '' },
	});

	if (!data?.getUserById) return null;

	const { username, name, email, createdAt, roles } = data.getUserById;

	return (
		<Row
			data-testid="Profile"
			align="middle"
			justify="center"
			style={{ display: 'flex', flex: 1 }}
		>
			<Col span={12}>
				<Typography.Title level={2}>Profile</Typography.Title>
				<Descriptions bordered column={1}>
					<Descriptions.Item label="Username">{username}</Descriptions.Item>
					{name && <Descriptions.Item label="Name">{name}</Descriptions.Item>}
					<Descriptions.Item label="Email">{email}</Descriptions.Item>
					<Descriptions.Item label="Roles">{roles}</Descriptions.Item>
					<Descriptions.Item label="Account Creation">
						{formatDate(createdAt)}
					</Descriptions.Item>
				</Descriptions>
			</Col>
		</Row>
	);
};
