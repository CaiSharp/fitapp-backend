import React from 'react';
import { MockedProvider, MockedResponse } from '@apollo/react-testing';

import { Profile } from './Profile';
import { ProfileDocument, Role } from '@fitapp/api/client';
import { render } from '@testing-library/react';

import { AuthContext } from 'context';

const mockAllData: MockedResponse[] = [
	{
		request: {
			query: ProfileDocument,
			variables: {
				id: 'testUserID',
			},
		},
		result: {
			data: {
				getUserById: {
					id: 'testUserID',
					username: 'AlinaFischer',
					name: 'Alina',
					email: 'alina@gmail.com',
					createdAt: new Date('10.20.2020').toISOString(),
					roles: ['USER'],
				},
			},
		},
	},
];

describe('<Profile>', () => {
	it('should render the user info if logged in', async () => {
		const { findByTestId, findByText } = render(
			<AuthContext.Provider
				value={{
					user: {
						email: 'test@email.com',
						username: 'testUsername',
						id: 'testUserID',
						roles: ['USER' as Role],
					},
					updateUser: jest.fn(),
					logout: jest.fn(),
				}}
			>
				<MockedProvider mocks={mockAllData} addTypename={false}>
					<Profile />
				</MockedProvider>
			</AuthContext.Provider>,
		);

		const profileElement = await findByTestId('Profile');
		const userName = await findByText('AlinaFischer');
		const name = await findByText('Alina');
		const email = await findByText('alina@gmail.com');
		const createdAt = await findByText('20/10/2020');
		const roles = await findByText('USER');

		expect(profileElement).toBeTruthy();
		expect(userName).toBeTruthy();
		expect(name).toBeTruthy();
		expect(email).toBeTruthy();
		expect(createdAt).toBeTruthy();
		expect(roles).toBeTruthy();
	});

	it("shouldn't render the user info if logged out/invalid id", () => {
		// prevents act waring here
		console.error = jest.fn();

		const { queryByText, queryByTestId } = render(
			<AuthContext.Provider
				value={{
					user: null,
					updateUser: jest.fn(),
					logout: jest.fn(),
				}}
			>
				<MockedProvider mocks={mockAllData} addTypename={false}>
					<Profile />
				</MockedProvider>
			</AuthContext.Provider>,
		);

		const componentTestId = queryByTestId('Profile');
		const userName = queryByText('AlinaFischer');
		const name = queryByText('Alina');
		const email = queryByText('alina@gmail.com');
		const createdAt = queryByText('20/10/2020');
		const roles = queryByText('USER');

		expect(componentTestId).toBeFalsy();
		expect(userName).toBeFalsy();
		expect(name).toBeFalsy();
		expect(email).toBeFalsy();
		expect(createdAt).toBeFalsy();
		expect(roles).toBeFalsy();
	});
});
