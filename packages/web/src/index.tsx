import ApolloClient from 'apollo-boost';
import { App } from 'App';
import { AuthContext, initUser, logout } from 'context/index';
import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { User } from 'services/sharedInterfaces';

import { ApolloProvider } from '@apollo/react-hooks';

const { REACT_APP_BACKEND_URL } = process.env;

const client = new ApolloClient({
	uri: REACT_APP_BACKEND_URL,
	request: async operation => {
		const token = localStorage.getItem('token');
		operation.setContext({
			headers: {
				authorization: token ? `Bearer ${token}` : '',
			},
		});
	},
});

const ApolloApp: React.FC = props => {
	const [authState, setAuthState]: [
		User | null,
		React.Dispatch<React.SetStateAction<User | null>>,
	] = useState(initUser());

	return (
		<AuthContext.Provider
			value={{
				user: authState,
				updateUser: (user: User | null) => setAuthState(user),
				logout: () => logout(setAuthState),
			}}
		>
			<BrowserRouter>
				<ApolloProvider client={client}>
					<App />
				</ApolloProvider>
			</BrowserRouter>
		</AuthContext.Provider>
	);
};

ReactDOM.render(<ApolloApp />, document.getElementById('root'));
