import React from 'react';
import { render } from '@testing-library/react';
import { App } from './App';
import { MemoryRouter } from 'react-router';

describe('<App/>', () => {
	it('renders and matches snapshot', () => {
		const { baseElement } = render(
			<MemoryRouter initialEntries={['/']}>
				<App />
			</MemoryRouter>,
		);
		expect(baseElement).toMatchSnapshot();
	});
});
