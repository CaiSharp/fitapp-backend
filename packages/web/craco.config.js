const CracoAntDesignPlugin = require('craco-antd');
const path = require('path');

module.exports = {
	plugins: [
		{
			plugin: CracoAntDesignPlugin,
			options: {
				customizeThemeLessPath: path.join(__dirname, './src/styles/style.less'),
			},
		},
	],
	babel: {
		presets: [],
		plugins: ['@babel/plugin-proposal-optional-chaining'],
	},
	jest: {
		configure(config) {
			config.transformIgnorePatterns = ['/node_modules/(?!antd)/.+\\.js$'];
			config.setupFilesAfterEnv = ['./jest.setup.js'];
			return config;
		},
	},
};
