window.matchMedia =
	window.matchMedia ||
	jest.fn(() => ({
		matches: false,
		addListener: jest.fn(),
		removeListener: jest.fn(),
	}));