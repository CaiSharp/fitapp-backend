module.exports = {
	extends: [
		'react-app',
		'eslint:recommended',
		'plugin:@typescript-eslint/eslint-recommended',
		'plugin:@typescript-eslint/recommended',
		'prettier',
		'prettier/react',
		'prettier/@typescript-eslint',
	],
	rules: {
		'@typescript-eslint/explicit-function-return-type': 'off',
		'@typescript-eslint/no-explicit-any': 'warn',
		'no-unneeded-ternary': ['warn', { defaultAssignment: false }],
		'object-shorthand': 'warn',
		'arrow-body-style': ['warn', 'as-needed'],
		'@typescript-eslint/no-non-null-assertion': 'error',
		'@typescript-eslint/no-empty-function': 'off',
	},
};
