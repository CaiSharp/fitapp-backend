[![pipeline status](https://gitlab.com/CaiSharp/fitapp-backend/badges/master/pipeline.svg)](https://gitlab.com/CaiSharp/fitapp-backend/-/commits/master)
[![coverage report](https://gitlab.com/CaiSharp/fitapp-backend/badges/master/coverage.svg)](https://gitlab.com/CaiSharp/fitapp-backend/-/commits/master)

# FitApp

## How to run

### Web & Server

1. install with `yarn`
2. run `yarn dev`
3. _done_

soon...

## Why did my commit fail?!

- We have a `pre-commit` & `commit-msg` hook in place which validate your commit.
- `pre-commit` runs `yarn lint` on the project and fails if ESLint detects an error
- 'commit-msg' runs `commitlint` over your commit message and validates if it adheres to conventional commits, you can read more about it here: https://www.conventionalcommits.org/en/v1.0.0/
